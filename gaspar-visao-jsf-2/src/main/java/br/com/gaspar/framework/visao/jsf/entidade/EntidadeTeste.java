package br.com.gaspar.framework.visao.jsf.entidade;


import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import br.com.gaspar.utils.entidade.EntidadeBase;
@Entity
@NamedQueries({
	@NamedQuery(name="EntidadeTeste.buscarTodos", query="from EntidadeTeste obj where obj.id = ?"),
	@NamedQuery(name="EntidadeTeste.buscarTodosAtivos", query="from EntidadeTeste obj where obj.id = ? and obj.ativo = true"),
	@NamedQuery(name="EntidadeTeste.naoDeveExistirNome", query="select count(*) from EntidadeTeste obj where obj.id = ?"),
	@NamedQuery(name="EntidadeTeste.naoDeveExistirAtivo", query="select count(*) from EntidadeTeste obj where obj.ativo = ?")
})
//@ExclusaoLogica
public class EntidadeTeste extends EntidadeBase {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7505077173666331335L;
	
	@Id
	private Long id;

	private String nome;
	
	private Boolean ativo = true;
	
	public EntidadeTeste(String nome){
		setNome(nome);
	}
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	@Override
	public void setId(Long id) {
		this.id = id; 		
	}

	@Override
	public Long getId() {
		return this.id;
	}

}
