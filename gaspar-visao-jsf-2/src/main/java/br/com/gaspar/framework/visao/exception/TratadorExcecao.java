package br.com.gaspar.framework.visao.exception;

import javax.ejb.EJBTransactionRolledbackException;
import javax.persistence.PersistenceException;
import javax.transaction.RollbackException;
import javax.validation.ConstraintViolationException;

import br.com.gaspar.utils.exception.BaseException;

public class TratadorExcecao {
	
	/**
	 * Trata a mensagem esperando interceptar uma ConstraintViolationException
	 * @param e
	 * @throws BaseException
	 */
	public static void tratarExcecao(Exception e) throws BaseException{
		if(e instanceof EJBTransactionRolledbackException){
			RollbackException rbe = (RollbackException) e.getCause();
			if (rbe.getCause() instanceof PersistenceException){
				PersistenceException pe  = (PersistenceException) rbe.getCause();
				if(pe.getCause() instanceof ConstraintViolationException){
					ConstraintViolationException cve = (ConstraintViolationException) pe.getCause();
					throw new BaseException(cve.getConstraintViolations());
				}
			}
		}
	}

}
