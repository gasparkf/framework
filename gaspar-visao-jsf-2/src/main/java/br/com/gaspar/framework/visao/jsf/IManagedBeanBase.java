package br.com.gaspar.framework.visao.jsf;


import org.primefaces.event.SelectEvent;

import br.com.gaspar.utils.exception.BaseException;



/**
 * Interface do ManagedBeanBase
 * 
 * @author gaspar
 *
 */
public interface IManagedBeanBase {
	
	void iniciar();
	
	String gravar();
	
	String criar();
	
	String listar();
	
	String cancelar();	
	
	String excluir();
	
	void selecionarEditar(SelectEvent event) throws BaseException;
	
	/*String selecionarMostrar();
	
	String selecionarExcluir();*/

}
