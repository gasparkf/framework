package br.com.gaspar.framework.visao.jsf.primefaces;


import java.util.List;

import javax.faces.model.ListDataModel;

import org.primefaces.model.SelectableDataModel;

import br.com.gaspar.utils.entidade.EntidadeBase;

/**
 * Data Model implementando a interface do PrimeFaces
 * @author gaspar
 *
 * @param <T>
 */
public class PrimeFacesDataModel<T extends EntidadeBase> extends ListDataModel<T> implements SelectableDataModel<T> {
	    
		  
	    public PrimeFacesDataModel() {  
	    }  
	  
	    public PrimeFacesDataModel(List<T> data) {  
	        super(data);  
	    }  
	      
	    @Override  
	    public T getRowData(String rowKey) {  
	        //In a real app, a more efficient way like a query by rowKey should be implemented to deal with huge data  
	          
	        @SuppressWarnings("unchecked")
			List<T> lista = (List<T>) getWrappedData();  
	          
	        for(T entidade : lista) {  
	            if(entidade.getId().toString().equals(rowKey))  
	                return entidade;  
	        }  
	          
	        return null;  
	    }  
	  
	    @Override  
	    public Object getRowKey(T entidade) {  
	        return entidade.getId();  
	    }  
}
