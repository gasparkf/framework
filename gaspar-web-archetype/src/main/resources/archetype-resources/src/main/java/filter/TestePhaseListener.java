#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.filter;

import javax.faces.context.FacesContext;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;

public class TestePhaseListener implements PhaseListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4161304336144724967L;

	@Override
	public void afterPhase(PhaseEvent event) {
		FacesContext facesContext = event.getFacesContext();
		if( facesContext.getViewRoot() != null){
		    String paginaAtual = facesContext.getViewRoot().getViewId();
		    System.out.println("--- Depois Fase ---");
		    System.out.println(paginaAtual);
		    System.out.println("--- ----------- ---");
		}
	}

	@Override
	public void beforePhase(PhaseEvent event) {
		FacesContext facesContext = event.getFacesContext();
		if( facesContext.getViewRoot() != null){
		    String paginaAtual = facesContext.getViewRoot().getViewId();
		    System.out.println("--- Antes Fase ---");
		    System.out.println(paginaAtual);
		    System.out.println("--- ----------- ---");
		}
	}

	@Override
	public PhaseId getPhaseId() {
		return PhaseId.RESTORE_VIEW;
	}

}
