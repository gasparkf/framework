#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.util;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

@FacesConverter(value="moedaConverter")
public class MoedaConverter implements Converter {

	private Locale locale = new Locale("pt", "BR");
	
	
	@Override
	public Object getAsObject(FacesContext facesContext,
			UIComponent uiComponent, String value) {

		if (value != null) {
			value = value.trim();
			if (value.length() > 0) {
				try {
					return new BigDecimal(NumberFormat.getNumberInstance(getLocale())
							.parse(value).doubleValue());
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}
		}
		return null;
	}

	@Override
	public String getAsString(FacesContext facesContext,
			UIComponent uiComponent, Object value) {

		if (value == null) {
			return "";
		}
		if (value instanceof String) {
			return (String) value;
		}
		try {
			NumberFormat formatador = NumberFormat.getNumberInstance(getLocale());
			formatador.setGroupingUsed(true);
			return formatador.format(value);

		} catch (Exception e) {
			throw new ConverterException("Formato não é número.");
		}
	}

	public Locale getLocale() {
		return locale;
	}

	public void setLocale(Locale locale) {
		this.locale = locale;
	}
}
