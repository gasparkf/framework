package br.com.carrera.mb;

import java.io.IOException;
import java.io.Serializable;
import java.util.Date;

import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.hibernate.validator.constraints.Length;
import org.omnifaces.util.Faces;
import org.omnifaces.util.Messages;

import ${package}.bo.EmailBO;
import ${package}.bo.UsuarioBO;
import ${package}.entidade.seguranca.Papel;
import ${package}.entidade.seguranca.Usuario;
import ${package}.util.Email;
import br.com.gaspar.utils.DataUtil;
import br.com.gaspar.utils.exception.BaseException;

@Named
@SessionScoped
public class LoginMB implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String email;
	
	private String senha;
	
	private Boolean exibirTrocarSenha = false;
	
	@Length(min = 4, message="Senha deve conter no mínimo 4 dígitos")
	private String novaSenha;
	
	@Length(min = 4, message="Senha deve conter no mínimo 4 dígitos")
	private String senhaRepetida;
	
	/**
	 * usado somente para a troca de senhas
	 */
	private Usuario usuarioTroca;
	
	/**
	 * usuário logado no sistema
	 */
	private Usuario usuarioLogado;
	
	@EJB
	private UsuarioBO bo;
	
	@EJB
	private EmailBO emailBO;
	
	private Boolean lembrar = false;
	
	public String login() throws IOException {
		
		try {
			
			SecurityUtils.getSubject().login(new UsernamePasswordToken(email.trim(), senha.trim(), lembrar));
			usuarioLogado = bo.buscarPorNamedQuery("Usuario.buscarPorEmail", email.trim());
			
			//Envia email de teste
			Email email = new Email();
			email.setAssunto("Venda Direta - Usuário logado: " + usuarioLogado.getEmail());
			email.setDestinatario("gasparkf@gmail.com");
			email.setMensagem("O usuário: " + usuarioLogado.getEmail() + " acaba de logar no sistema as: " + DataUtil.getDate(new Date()));
			email.setRemetente("Venda Direta <vendadireta@carrerasistemas.com.br>");
			
			emailBO.enviarEmail(email);
			
			if(usuarioLogado.getSenhaTemporaria()){
				usuarioTroca = usuarioLogado;
				usuarioLogado = null;
				exibirTrocarSenha = true;
				return null;
			}
			
			if(usuarioLogado.getPapeis().contains(Papel.CLIENTE)){
				Faces.redirect("publico/assistenteFinanceiro.xhtml");
				return null;
			}
			
			
			if(usuarioLogado.getPapeis().contains(Papel.ADMIN)){
				//Faces.redirect("admin/usuario.xhtml");
				Faces.redirect("publico/assistenteFinanceiro.xhtml");
				return null;
			}
			
			//Guarda a última URL acessada pelo usuário e redirecionada para ela
            //SavedRequest savedRequest = WebUtils.getAndClearSavedRequest(Faces.getRequest());
			//Faces.redirect(savedRequest != null ? savedRequest.getRequestUrl() : "admin/index.xhtml");
			Faces.redirect("publico/cliente.xhtml");
        }
        catch (AuthenticationException e) {
        	Messages.addGlobalError("Usuário desconhecido, tente novamente");
            e.printStackTrace(); // TODO: logger.
        } catch (BaseException e) {
        	Messages.addGlobalError("Erro ao buscar por CNPJ/CPF, contate o suporte!");
			e.printStackTrace();
		}
		return null;
	}
		
	public void logout() throws IOException {
		SecurityUtils.getSubject().logout();
	    Faces.invalidateSession();
	    Faces.redirect("login.xhtml");
	}
	
	public String confirmarSenha(){
		try {
			if(!getNovaSenha().equals(getSenhaRepetida())){
				Messages.addGlobalError("As senhas não estão iguais, digite-as novamente.");
				return null;
			}
			usuarioTroca.setSenha(novaSenha);
			usuarioLogado = bo.trocarSenhaTemporaria(usuarioTroca);
			exibirTrocarSenha  = false;
			this.email = usuarioLogado.getEmail();
			this.senha = this.novaSenha;
			
			return login();
		} catch (IOException e) {
			Messages.addGlobalError("Erro ao trocar senha, contate o suporte.");
		} catch (BaseException e) {
			Messages.addGlobalError("Erro ao trocar senha, contate o suporte.");
		}
		
		return "login?faces-redirect=true";
	}
	
	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public Usuario getUsuarioLogado() {
		return usuarioLogado;
	}

	public void setUsuarioLogado(Usuario usuarioLogado) {
		this.usuarioLogado = usuarioLogado;
	}

	public Boolean getLembrar() {
		return lembrar;
	}

	public void setLembrar(Boolean lembrar) {
		this.lembrar = lembrar;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNovaSenha() {
		return novaSenha;
	}

	public void setNovaSenha(String novaSenha) {
		this.novaSenha = novaSenha;
	}

	public String getSenhaRepetida() {
		return senhaRepetida;
	}

	public void setSenhaRepetida(String senhaRepetida) {
		this.senhaRepetida = senhaRepetida;
	}

	public Usuario getUsuarioTroca() {
		return usuarioTroca;
	}

	public void setUsuarioTroca(Usuario usuarioTroca) {
		this.usuarioTroca = usuarioTroca;
	}

	public Boolean getExibirTrocarSenha() {
		return exibirTrocarSenha;
	}

	public void setExibirTrocarSenha(Boolean exibirTrocarSenha) {
		this.exibirTrocarSenha = exibirTrocarSenha;
	}
}
