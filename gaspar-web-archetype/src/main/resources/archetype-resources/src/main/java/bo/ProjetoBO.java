#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.bo;

import br.com.gaspar.framework.modelo.BaseBO;
import br.com.gaspar.utils.entidade.EntidadeBase;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import ${package}.bo.ProjetoBO;
import ${package}.dao.ProjetoDAO;

@Stateless
public class ProjetoBO<T extends EntidadeBase> extends BaseBO<T> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@EJB
	private ProjetoDAO<T> dao;

	@Override
	public ProjetoDAO<T> getDAOPadrao() {
		return dao;
	}

}
