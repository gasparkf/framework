package ${package}.bo;

import java.util.Date;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.annotation.Resource;
import javax.ejb.Asynchronous;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.mail.Address;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;

import br.com.carrera.entidade.seguranca.Usuario;
import br.com.carrera.util.Email;
import br.com.gaspar.utils.DataUtil;
import br.com.gaspar.utils.exception.BaseException;

@Stateless
public class EmailBO {
	
	@Resource(mappedName="java:jboss/mail/MailVendaDireta")
    private Session mailSession;
	
	@EJB
	private UsuarioBO usuarioBO;
	
	@Asynchronous
	public void enviarEmail(Email email) throws BaseException{
		try{
			MimeMessage message = new MimeMessage(mailSession);
			Address from = new InternetAddress(email.getRemetente());
			Address[] to = new InternetAddress[] {new InternetAddress(email.getDestinatario()) };
	    		
			message.setFrom(from);
			message.setRecipients(Message.RecipientType.TO, to);
			message.setSubject(email.getAssunto());
			message.setSentDate(new java.util.Date());
			
			if(email.getTipoEmail().equals("html")){
				message.setContent(email.getMensagem(), "text/html; charset=utf-8");
			}else{
				message.setContent(email.getMensagem(),"text/plain; charset=utf-8");
			}
			
			if(email.getArquivoAnexoBA() != null){
				BodyPart messageBodyPart = new MimeBodyPart();
				BodyPart textoBP = new MimeBodyPart();
				textoBP.setText(email.getMensagem());
				
				Multipart multipart = new MimeMultipart();
				
				String fileName = "Venda Direta - " + DataUtil.getDate(new Date(), "dd/MM/yyyy HH:mm");
		        DataSource source = new ByteArrayDataSource(email.getArquivoAnexoBA(), "text/html");
		        messageBodyPart.setDataHandler(new DataHandler(source));
		        messageBodyPart.setFileName(fileName);
		        
		        
		        multipart.addBodyPart(messageBodyPart);
			    multipart.addBodyPart(textoBP);
		
			    message.setContent(multipart);
				}
				
			Transport.send(message);
			// log.debug("Email Enviado!");
		} catch (MessagingException e) {
			e.printStackTrace();
			Usuario usuario = email.getUsuario();
			usuario.setErro(e.getMessage());
			usuario.setEnviado(true);
				
			usuarioBO.editar(usuario);
		}
		
	}
	
	public Session getMailSession() {
		return mailSession;
	}

	public void setMailSession(Session mailSession) {
		this.mailSession = mailSession;
	}

}
