#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.dao;

import br.com.gaspar.framework.persistencia.jpa.BaseDAO;
import br.com.gaspar.utils.entidade.EntidadeBase;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import ${package}.dao.ProjetoDAO;

@Stateless
public class ProjetoDAO<T extends EntidadeBase> extends BaseDAO<T> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@PersistenceContext(unitName="projeto-pu")
	@Override
	protected void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

}
