#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.entidade.seguranca;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

import br.com.gaspar.utils.entidade.InquilinoEntidadeBase;

@Entity
@NamedQueries({
	@NamedQuery(name="Usuario.buscarTodos", query="select obj from Usuario obj order by obj.nome"),
	@NamedQuery(name="Usuario.buscarPorEmail", query="select obj from Usuario obj where obj.email = :p0"),
	@NamedQuery(name="Usuario.naoDeveExistirEmail", query="select count(obj.id) from Usuario obj where obj.email = :p0")
	
})
public class Usuario extends InquilinoEntidadeBase{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="Usuario", sequenceName="usuario_seq", allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.AUTO, generator="Usuario")
	private Long id;
	
	@NotEmpty(message="Obrigatório informar o nome!")
	@NotNull(message="Obrigatório informar o nome!")
	@Column(name="nome")
	private String nome;
	
	@NotEmpty(message="Obrigatório informar o e-mail!")
	@NotNull(message="Obrigatório informar e-mail!")
	@Length(max=255, message="Máximo de 255 caracteres para o email!")
	@Column(nullable=false, length=255)
	private String email;
	
	@NotEmpty(message="Obrigatório informar a senha!")
	@NotNull(message="Obrigatório informar a senha!")
	@Length(max=255, message="Máximo de 255 caracteres para a senha!")
	@Column(nullable=false, length=255)
	private String senha;
	
	@Column(name="senha_temporaria")
	private Boolean senhaTemporaria = true;
	
	private String salt;
	
	private Boolean ativo = true;
	
	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
	@JoinTable(name="usuario_papel", joinColumns= @JoinColumn(name="usuario_id"), inverseJoinColumns = @JoinColumn(name = "papel_id"))
	private Set<Papel> papeis;
	
	/*@ManyToMany
	@JoinTable(name="usuario_permissao", joinColumns= @JoinColumn(name="usuario_id"), inverseJoinColumns = @JoinColumn(name = "permissao_id"))
	private Set<Permissao> permissoes;*/
	
	private String senhaInicial;
	
	private String erro;
	
	private Boolean enviado = false;
	
	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public Usuario(){
		
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Usuario other = (Usuario) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public Boolean getSenhaTemporaria() {
		return senhaTemporaria;
	}

	public void setSenhaTemporaria(Boolean senhaTemporaria) {
		this.senhaTemporaria = senhaTemporaria;
	}

	public String getSalt() {
		return salt;
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}

	public Set<Papel> getPapeis() {
		return papeis;
	}

	public void setPapeis(Set<Papel> papeis) {
		this.papeis = papeis;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSenhaInicial() {
		return senhaInicial;
	}

	public void setSenhaInicial(String senhaInicial) {
		this.senhaInicial = senhaInicial;
	}

	public String getErro() {
		return erro;
	}

	public void setErro(String erro) {
		this.erro = erro;
	}

	public Boolean getEnviado() {
		return enviado;
	}

	public void setEnviado(Boolean enviado) {
		this.enviado = enviado;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
}
