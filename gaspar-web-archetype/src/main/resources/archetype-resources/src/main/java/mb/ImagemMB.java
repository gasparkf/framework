package br.com.carrera.mb;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.Serializable;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;

import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import br.com.gaspar.utils.StringUtil;

@ManagedBean
@ApplicationScoped
public class ImagemMB implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static final String CAMINHO_IMAGEM = "/carrera/flamingos/";
	
	public StreamedContent getImagem() throws IOException{
		FacesContext ctx = FacesContext.getCurrentInstance();
		DefaultStreamedContent content = new DefaultStreamedContent();
		content.setContentType("image/jpg");
		
		if(!ctx.getRenderResponse()){
			String nome = ctx.getExternalContext().getRequestParameterMap().get("img");
			if(!StringUtil.ehBrancoOuNulo(nome)){
				content.setStream(new FileInputStream(System.getProperty("user.home") + CAMINHO_IMAGEM + nome));
			}
		}
		return content;
	}

}
