package ${package}.util;

import br.com.carrera.entidade.seguranca.Usuario;

public class Email {
	
	private String remetente;
    
	private String destinatario;
    
    private String assunto;
    
    private String mensagem;
    
    private String tipoEmail = "text";
    
    private byte[] arquivoAnexoBA;
    
    private Usuario usuario;
    
    public Email(){
    	
    }

	public Email(String remetente, String destinatario, String assunto,
			String mensagem, String tipoEmail) {
		super();
		this.remetente = remetente;
		this.destinatario = destinatario;
		this.assunto = assunto;
		this.mensagem = mensagem;
		this.tipoEmail = tipoEmail;
	}

	public String getRemetente() {
		return remetente;
	}

	public void setRemetente(String remetente) {
		this.remetente = remetente;
	}

	public String getDestinatario() {
		return destinatario;
	}

	public void setDestinatario(String destinatario) {
		this.destinatario = destinatario;
	}

	public String getAssunto() {
		return assunto;
	}

	public void setAssunto(String assunto) {
		this.assunto = assunto;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	public String getTipoEmail() {
		return tipoEmail;
	}

	public void setTipoEmail(String tipoEmail) {
		this.tipoEmail = tipoEmail;
	}

	public byte[] getArquivoAnexoBA() {
		return arquivoAnexoBA;
	}

	public void setArquivoAnexoBA(byte[] arquivoAnexoBA) {
		this.arquivoAnexoBA = arquivoAnexoBA;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}


}
