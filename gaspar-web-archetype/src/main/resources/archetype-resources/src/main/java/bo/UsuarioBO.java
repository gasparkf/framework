package ${package}.bo;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.crypto.hash.Sha256Hash;

import ${package}.dao.ProjetoDAO;
import ${package}.entidade.seguranca.Usuario;
import ${package}.util.Email;
import br.com.gaspar.framework.modelo.BaseBO;
import br.com.gaspar.utils.exception.BaseException;

@Stateless
public class UsuarioBO extends BaseBO<Usuario>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@EJB
	private ProjetoDAO<Usuario> dao;
	
	@EJB
	private EmailBO emailBO;

	@Override
	public ProjetoDAO<Usuario> getDAOPadrao() {
		return dao;
	}
	
	public List<Usuario> buscarTodosPorNomeLike(String nome){
		List<Usuario> lista = null;
		StringBuilder hql = new StringBuilder("select obj from Usuario obj");
		hql.append(" where obj.inquilino = :p0 and lower(obj.nome) like :p1");
		hql.append(" order by obj.nome");
		
		lista = getDAOPadrao().getEntityManager().createQuery(hql.toString(), Usuario.class)
				.setParameter("p0", SecurityUtils.getSubject().getPrincipal().toString())
				.setParameter("p1", "%" + nome.toLowerCase() + "%")
				.getResultList();
		
		return lista;
	}
	
	@Override
	protected void gravarAntes(Usuario entidade) throws BaseException {
		if(entidade.getSenhaTemporaria()){
			String senhaGerada = gerarSenha(); 
			entidade.setSenha(senhaGerada);
		}
		entidade.setSenhaInicial(entidade.getSenha());
		//Faz o Hash da senha
		entidade.setSenha(new Sha256Hash(entidade.getSenha()).toHex());
	}
	
	@Override
	protected void gravarApos(Usuario entidade) throws BaseException {
		entidade.setInquilino(entidade.getEmail());
		emailBO.enviarEmail(
				new Email(
						"carreraconsultoria@gmail.com", 
						//"gasparkf@gmail.com",
						entidade.getEmail(), 
						"Carrera Sistemas - Venda Direta",
						"Olá, seja bem vindo ao sistema Venda Direta, você está recebendo seu usuário e senha personalizados. \n"
						+ "De agora em diante controlar o seu negócio ficará muito mais fácil e prático através da nossa ferramenta. \n"
						+ "Durante o primeiro acesso será requisitada a troca da senha inicial, lembre-se de guardá-la em lugar seguro. \n"
						+ "Usuário: " + entidade.getEmail() +"\n"
						+ "Senha: " + entidade.getSenhaInicial() +"\n"
						+ "Segue o link para sistema: http://www.vendadireta.carrerasistemas.com.br copie e cole no navegador Google Chrome." +"\n \n"
						+ "Desejamos a você bons negócios! \n\n"
						+ "Carrera Consultoria e Sistemas \n"
						+ "José Gaspar Kuhnen Filho - Diretor \n"
						+ "67 - 9272-0365",
						"text"
				)
			);
	}
	
	/**
	 *Gera a senha inicial ou reseta a senha do usuário 
	 * @return
	 */
	public String gerarSenha(){
		String[] carct ={"0","1","2","3","4","5","6","7","8","9",
				"a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"};
				//"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"}; 

		String senha=""; 

		for(int x=0; x < 6; x++){ 
			int j = (int) (Math.random()*carct.length); 
			senha += carct[j]; 
		} 

		return senha; 
	}
	
	/**
	 * 
	 * @param usuario
	 * @return
	 * @throws BaseException 
	 */
	public Usuario trocarSenhaTemporaria(Usuario usuario) throws BaseException{
		usuario.setSenha(new Sha256Hash(usuario.getSenha()).toHex());
		usuario.setSenhaTemporaria(false);
		usuario.setSenhaInicial(null);
		
		editar(usuario);
		return usuario;
	}

}
