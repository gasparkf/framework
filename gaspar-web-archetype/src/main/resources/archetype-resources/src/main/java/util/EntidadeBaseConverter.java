#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.util;

import br.com.gaspar.utils.entidade.EntidadeBase;

import java.util.HashMap;
import java.util.Map;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

@FacesConverter(value="entidadeConverter", forClass=EntidadeBase.class)
public class EntidadeBaseConverter implements Converter {
	
	private static Map<Long, EntidadeBase> cache = new HashMap<Long, EntidadeBase>();   
	
	@Override   
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String arg2) {   
		EntidadeBase obj = null;
		 if (arg2 != null) {   
			 try {   
		    	 obj = cache.get(Long.parseLong(arg2));   
		     } catch (NumberFormatException e) {   
		    	 throw new ConverterException("Valor inválido: " + arg2, e);   
		     }  
		 }   
	    return obj;
	}   

	@Override   
	public String getAsString(FacesContext arg0, UIComponent arg1, Object arg2) {   
		if(arg2 == null){
			return "0";
		}
		EntidadeBase entidade = (EntidadeBase) arg2;   
        Long id = entidade.getId();   
        if (id != null) {   
            cache.put(id, entidade);   
            return String.valueOf(id.longValue());   
        } else {   
            return "0";   
        }      
	}   
}
