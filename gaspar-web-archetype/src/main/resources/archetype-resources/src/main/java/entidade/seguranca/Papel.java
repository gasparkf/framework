#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.entidade.seguranca;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import br.com.gaspar.utils.entidade.EntidadeBase;


@Entity
@NamedQueries({
	@NamedQuery(name="Papel.buscarTodos", query="select obj from Papel obj order by obj.descricao")
})
public class Papel extends EntidadeBase{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
		
	public static final Papel SU = new Papel(1L, "SU");
	public static final Papel ADMIN = new Papel(2L, "ADMIN ");
	public static final Papel CLIENTE = new Papel(3L, "CLIENTE");
	public static final Papel VENDEDOR = new Papel(4L, "VENDEDOR");
	public static final Papel TELEMARKETING = new Papel(5L, "TELEMARKETING");

	@Id
	@SequenceGenerator(name = "Papel", sequenceName = "papel_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "Papel")
	private Long id;

	@NotEmpty
	@NotNull
	private String descricao;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public Papel(){
		
	}
	
	public Papel(Long id, String descricao) {
		super();
		this.id = id;
		this.descricao = descricao;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Papel other = (Papel) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	@Override
	public String toString() {
		return getDescricao();
	}
}
