package br.com.gaspar.framework.visao.jsf.converter;


import java.util.HashMap;
import java.util.Map;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import br.com.gaspar.utils.entidade.iface.IEntidadeBase;



//@FacesConverter(value="entidadeConverter", forClass=IEntidadeBase.class)
public class EntidadeBaseConverter implements Converter {
	
	private static Map<String, IEntidadeBase> cache = new HashMap<String, IEntidadeBase>();   
	
	@Override   
	public Object getAsObject(FacesContext facesContext, UIComponent componenteJSF, String idCache) {   
		IEntidadeBase obj = null;
		if (idCache != null) {
			  obj = cache.get(idCache);
		}   
	    return obj;
	}   

	@Override   
	public String getAsString(FacesContext facesContext, UIComponent componenteJSF, Object entidade) {   
		if(entidade == null){
			return "0";
		}
		IEntidadeBase IEntidadeBase = (IEntidadeBase) entidade;   
        Long id = IEntidadeBase.getId();
        String nomeClasse = IEntidadeBase.getClass().getSimpleName();
        String idCache = nomeClasse + id;
        if (id != null) {
        	if( !cache.containsKey(idCache)){
        		cache.put(idCache, IEntidadeBase);
        	}
            return idCache;   
        } else {   
            return "0";   
        }      
	}   
}

