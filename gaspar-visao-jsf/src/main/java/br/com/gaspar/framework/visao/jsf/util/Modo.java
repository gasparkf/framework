package br.com.gaspar.framework.visao.jsf.util;

/**
 * 
 * @author gaspar
 *
 */
public enum Modo {
	
	LISTAR,
	EDITAR,
	VISUALIZAR;

}
