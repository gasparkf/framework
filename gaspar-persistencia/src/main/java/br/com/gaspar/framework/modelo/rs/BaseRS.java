package br.com.gaspar.framework.modelo.rs;


import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.gaspar.framework.modelo.IBaseBO;
import br.com.gaspar.utils.entidade.iface.IEntidadeBase;
import br.com.gaspar.utils.exception.BaseException;


/**
 * Implementação base para os métodos de negócio comuns a todos os sistemas
 * @author gaspar
 */
public abstract class BaseRS<T extends IEntidadeBase>{
	
	protected static Logger log = LoggerFactory.getLogger(BaseRS.class);
	
	public abstract IBaseBO<T> getBO();
	
	public abstract String getNamedQuery();
	
	protected Class<T> classePersistente;
	
	public abstract void setClassePersistente();
	
	public BaseRS(){
		setClassePersistente();
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON+";charset=UTF-8")
	public List<T> buscarTodosPorNamedQuery() throws BaseException {
		List<T> lista = getBO().buscarTodosPorNamedQuery(getNamedQuery());
			
		return lista;
	}
	
	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON+";charset=UTF-8")
	public T buscarPorId(@PathParam("id") String id) throws BaseException {
		T entidade = getBO().buscarPorId(getClassePersistente(), Long.parseLong(id));
		return entidade;
	}
	
	@PUT
	@Produces(MediaType.APPLICATION_JSON+";charset=UTF-8")
	@Consumes(MediaType.APPLICATION_JSON+";charset=UTF-8")
	public T editar(T entidade) throws BaseException{
		
		editarAntes(entidade);
		
		entidade = getBO().editar(entidade);
		
		editarApos(entidade);
		
		return entidade;
		
	}
	
	/**
	 * Desing Pattern Template Method, disparado sempre antes de uma edição
	 * @param entidade
	 * @throws BaseException
	 * @author gaspar
	 */
	protected void editarAntes(T entidade) throws BaseException{
		
	}
	
	/**
	 * Desing Pattern Template Method, disparado sempre após uma edição
	 * @param entidade
	 * @throws BaseException
	 * @author gaspar
	 */
	protected void editarApos(T entidade) throws BaseException{
		
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON+";charset=UTF-8")
	public void gravar(T entidade) throws BaseException {
		
			gravarAntes(entidade);
			
			getBO().gravar(entidade);
			
			gravarApos(entidade);
	}
	
	/**
	 * Desing Pattern Template Method, disparado sempre antes de uma inclusão
	 * @param entidade
	 * @throws BaseException
	 * @author gaspar
	 */
	protected void gravarAntes(T entidade) throws BaseException{
		
	}

	/**
	 * Desing Pattern Template Method, disparado sempre após uma inclusão
	 * @param entidade
	 * @throws BaseException
	 * @author gaspar
	 */
	protected void gravarApos(T entidade) throws BaseException{
		
	}

	/**
	 * 
	 *//*
	@DELETE
	@Path("{id}")
	public void excluir(@PathParam("id") Long id) throws BaseException {
		
		excluirAntes(entidade);
			
		getBO().excluir(T, id);
			
		excluirApos(entidade);
		
		
	}*/
	
	/**
	 * Desing Pattern Template Method, disparado sempre antes de uma exclusão
	 * @param entidade
	 * @throws BaseException
	 * @author gaspar
	 */
	protected void excluirAntes(T entidade) throws BaseException{
		
	}
	
	/**
	 * Desing Pattern Template Method, disparado sempre após uma exclusão
	 * @param entidade
	 * @throws BaseException
	 * @author gaspar
	 */
	protected void excluirApos(T entidade) throws BaseException{
		
	}
	
	/**
	 * Desing Pattern Template Method, disparado sempre antes de uma exclusão
	 * @param entidade
	 * @throws BaseException
	 * @author gaspar
	 */
	protected void excluirAntes(Class<T> entidade) throws BaseException{
		
	}
	
	/**
	 * Desing Pattern Template Method, disparado sempre após uma exclusão
	 * @param entidade
	 * @throws BaseException
	 * @author gaspar
	 */
	protected void excluirApos(Class<T> entidade) throws BaseException{
		
	}

	public Class<T> getClassePersistente() {
		return classePersistente;
	}
}
