package br.com.gaspar.framework.persistencia.util;


/**
 * @author chsilva
 * @version 1.0
 * @created 15-set-2009 15:07:17
 */
public class JPAParams<T> implements IJPAParams<T> {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String sql;
    private Boolean nativeQuery;
    private Object[] params;
    private Integer paginaAtual;
    private Integer tamanhoPagina;
    private Class<T> classePersistente;
    private String orderBy;

    public JPAParams() {

    }

    public JPAParams(String sql, boolean nativeQuery, Object[] params) {
        this.sql = sql;
        this.nativeQuery = nativeQuery;
        this.params = params;
    }

    public JPAParams(String sql, boolean nativeQuery, Object[] params,
            Integer paginaAtual, Integer tamanhoPagina, Class<T> classePersistente, String orderBy) {

        this.sql = sql;
        this.nativeQuery = nativeQuery;
        this.params = params;
        this.paginaAtual = paginaAtual;
        this.tamanhoPagina = tamanhoPagina;
        this.classePersistente = classePersistente;
        this.orderBy = orderBy;
    }

    /**
     * 
     * @return
     */
    public Boolean isPaginado() {
        return (this.paginaAtual != null && this.tamanhoPagina != null);
    }

    /*
     * @return the sql
     */
    public String getSql() {
        return sql + (this.orderBy != null ? " " + this.orderBy : "");
    }

    /**
     * @param sql the sql to set
     */
    public void setSql(String sql) {
        this.sql = sql;
    }

    /**
     * @return the nativeQuery
     */
    public Boolean isNativeQuery() {
        return nativeQuery;
    }

    /**
     * @param nativeQuery the nativeQuery to set
     */
    public void setNativeQuery(boolean nativeQuery) {
        this.nativeQuery = nativeQuery;
    }

    /**
     * @return the params
     */
    public Object[] getParams() {
        return params;
    }

    /**
     * @param params the params to set
     */
    public void setParams(Object[] params) {
        this.params = params;
    }

    /**
     * @return the orderBy
     */
    public String getOrderBy() {
        return orderBy;
    }

    /**
     * @param orderBy the orderBy to set
     */
    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }
    
    public Integer getPaginaAtual() {
		return paginaAtual;
	}

	public void setPaginaAtual(Integer paginaAtual) {
		this.paginaAtual = paginaAtual;
	}

	public Integer getTamanhoPagina() {
		return tamanhoPagina;
	}

	public void setTamanhoPagina(Integer tamanhoPagina) {
		this.tamanhoPagina = tamanhoPagina;
	}

	public Class<T> getClassePersistente() {
		return classePersistente;
	}

	public void setClassePersistente(Class<T> classePersistente) {
		this.classePersistente = classePersistente;
	}

}
