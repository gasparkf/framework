/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.gaspar.framework.persistencia.util;

import java.io.Serializable;

/**
 *
 * @author chsilva
 */
public interface IJPAParams<T> extends Serializable{

    /**
     * @return the clazzResult
     */
    Class<T> getClassePersistente();

    /**
     * 
     * @return
     */
    Integer getPaginaAtual();

    /**
     * @return the orderBy
     */
    String getOrderBy();

    /**
     * 
     * @return
     */
    Integer getTamanhoPagina();

    /**
     * @return the params
     */
    Object[] getParams();

    /**
     * 
     * @return
     */
    String getSql();

    /**
     * @return the nativeQuery
     */
    Boolean isNativeQuery();

    /**
     *
     * @return
     */
    Boolean isPaginado();

    /**
     * 
     * @param classePersistente
     */
    void setClassePersistente(Class<T> classePersistente);

    /**
     * 
     * @param paginaAtual
     */
    void setPaginaAtual(Integer paginaAtual);

    /**
     * @param nativeQuery the nativeQuery to set
     */
    void setNativeQuery(boolean nativeQuery);

    /**
     * @param orderBy the orderBy to set
     */
    void setOrderBy(String orderBy);

    /**
     * 
     * @param tamanhoPagina
     */
    void setTamanhoPagina(Integer tamanhoPagina);

    /**
     * @param params the params to set
     */
    void setParams(Object[] params);

    /**
     * @param sql the sql to set
     */
    void setSql(String sql);

}
