package br.com.gaspar.framework.modelo;


import java.io.Serializable;
import java.util.List;

import br.com.gaspar.framework.persistencia.IBaseDAO;
import br.com.gaspar.framework.persistencia.util.IJPAParams;
import br.com.gaspar.utils.entidade.iface.IEntidadeBase;
import br.com.gaspar.utils.exception.BaseException;
/**
 * 
 * Interface base para métodos de negócio comuns a todos os sistemas
 * @author gaspar
 */
 public interface IBaseBO<T extends IEntidadeBase> extends Serializable {
	
	// Interfaces de gravação, edição e deleção
	 void gravar(T entidade) throws BaseException;
	
	 T editar(T entidade) throws BaseException;
	
	 //void excluir(Object entidade) throws SefazException;
	
	 void excluir(Class<T> entidade, Long id) throws BaseException;
	
	// Interfaces de metodos de pesquisa
	 T buscarPorId(Class<T> classePersistente, Long id) throws BaseException;
	
	 T buscarPorNamedQuery(String namedQuery, Object... params) throws BaseException ;
    
	 T buscarPorNativeQuery(Class<T> clazz, String sql, Object... params) throws BaseException ;
	
	 T buscarPorNativeQuery(String sql, Object... params) throws BaseException ;
    
	 List<T> buscarTodosPorNamedQuery(String namedQuery, Object... params) throws BaseException ;
    
	 List<T> buscarTodosPorNamedQuery(String namedQuery, Integer inicio, Integer tamanhoPagina, Object... params) throws BaseException ;
    
	 List<T> buscarTodosPorNativeQuery(String sql, Object... params) throws BaseException;
    
	 List<T> buscarTodosPorNativeQuery(Class<T> clazz, String sql, Object... params) throws BaseException;
    
	 List<T> buscarTodosPorNativeQuery(Class<T> clazz, String sql, Integer inicio, Integer tamanhoPagina, Object... params) throws BaseException;
    
	 List<T> buscarTodosPorNativeQuery(String sql, String nomeImplicito, Object... params) throws BaseException;
	
	 T buscarEntidade(IJPAParams<T> params) throws BaseException;

	 List<T> buscarTodos(IJPAParams<T> params) throws BaseException;
	
	 List<T> buscarTodos(Class<T> clazz, String ordenacao) throws BaseException;
	
	 Boolean usaExclusaoLogica(T entidade) throws BaseException;
	
	 T buscarPorNativeQuery(String sql, String nomeResultSetMapping, Object... params) throws BaseException;
	 
	 Long buscarCount(Class<T> clazz) throws BaseException;
	 
	 List<T> buscarTodos(Class<T> clazz, String ordenacao, Integer inicio, Integer tamanhoPagina) throws BaseException;
	 
	 IBaseDAO<T> getDAOPadrao();

	void excluir(T entidade) throws BaseException;
	
	Object buscarPorNamedQuery2(String namedQuery, Object... params) throws BaseException;
	
//	T buscarPorId(Long id) throws BaseException;
	
	Object buscarCount(String namedQuery, Object... params) throws BaseException;
	
	Long buscarCount2(String query) throws BaseException;
}
