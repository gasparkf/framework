/*
 * JPAUtil.java
 *
 * Created on 28 de Mar�o de 2008, 10:08
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package br.com.gaspar.framework.persistencia.util;

import javax.persistence.Query;

/**
 *
 * @author Christiano
 */
public class JPAUtil {
	
	/**
     * Preenche os parametros da query com os valores encontrados em no objeto params.
     * Quando construir as query procurar nomear os parametros com a seguinte notao:
     * p<n> onde <n> e igual um numero inteiro comencado por com 0 zero.
     * Exemplos de paramentros validos : :p0, :p1, :p2, :p3 etc. Observe que os dois pontos
     * na query significa que o nome e de um parametro.
     * @param query
     * @param params
     */
    public static void setParameters(Query query, Object... params) {
        if (params != null) {
            for (int i = 0; i < params.length; i++) {
                if (params[i] != null) {
                	query.setParameter("p" + i, params[i]);
                }
            }
        }
    }
    
    public static void setParametersNativeQuery(Query query, Object... params) {
        if (params != null) {
            for (int i = 0; i < params.length; i++) {
                if (params[i] != null) {
                	query.setParameter(i, params[i]);
                }
            }
        }
    }
}
