package br.com.carrerasistemas.entidade;

import java.util.ArrayList;
import java.util.List;

public class Entidade {
	
	private String caminho;
	
	private String pacoteBase;
	
	private String nome;
	
	private String bo;
	
	private String mb;
	
	private String xhtml;
	
	private List<Atributo> atributos = new ArrayList<Atributo>();

	public Entidade() {
		super();
	}

	public String getPacoteBase() {
		return pacoteBase;
	}

	public void setPacoteBase(String pacoteBase) {
		this.pacoteBase = pacoteBase;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getMb() {
		return mb;
	}

	public void setMb(String mb) {
		this.mb = mb;
	}

	public String getXhtml() {
		return xhtml;
	}

	public void setXhtml(String xhtml) {
		this.xhtml = xhtml;
	}

	public List<Atributo> getAtributos() {
		return atributos;
	}

	public void setAtributos(List<Atributo> atributos) {
		this.atributos = atributos;
	}

	public String getBo() {
		return bo;
	}

	public void setBo(String bo) {
		this.bo = bo;
	}

	public String getCaminho() {
		return caminho;
	}

	public void setCaminho(String caminho) {
		this.caminho = caminho;
	}
	
	

}
