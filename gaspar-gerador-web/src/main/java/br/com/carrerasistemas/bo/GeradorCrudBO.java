package br.com.carrerasistemas.bo;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.ejb.Stateless;

import org.omnifaces.util.Faces;

import br.com.carrerasistemas.entidade.Entidade;
import br.com.carrerasistemas.util.FreeMarkerUtil;
import br.com.gaspar.utils.ArquivoUtil;
import freemarker.template.TemplateException;

@Stateless
public class GeradorCrudBO{

	public void gerar(Entidade entidade){
		
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("entidade", entidade);
		
		try {
			String classeEntidade = FreeMarkerUtil.processarTemplate(parametros, "entidade.ftl", Faces.getServletContext());
			
			String classeBO = FreeMarkerUtil.processarTemplate(parametros, "bo.ftl", Faces.getServletContext());
			
			String classeMB = FreeMarkerUtil.processarTemplate(parametros, "mb.ftl", Faces.getServletContext());
			
			String arquivoXHTML = FreeMarkerUtil.processarTemplate(parametros, "xhtml.ftl", Faces.getServletContext());
			
			File aClasse = new File(montarCaminho(entidade, "entidade", entidade.getNome()+".java"));
			File aBO = new File(montarCaminho(entidade, "bo",entidade.getBo()+".java"));
			File aMB = new File(montarCaminho(entidade, "mb",entidade.getMb()+".java"));
			File aXHTML = new File(montarCaminhoXHTML(entidade, entidade.getXhtml()+".xhtml"));
			
			ArquivoUtil.salvar(aClasse, classeEntidade);
			ArquivoUtil.salvar(aBO, classeBO);
			ArquivoUtil.salvar(aMB, classeMB);
			ArquivoUtil.salvar(aXHTML, arquivoXHTML);
		
		} catch (TemplateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} 
	}
	
	private String montarCaminho(Entidade entidade, String pacote, String artefato){
		StringBuilder caminho = new StringBuilder(entidade.getCaminho());
		caminho.append(File.separator); 
		caminho.append("java");
		caminho.append(File.separator);
		caminho.append(entidade.getPacoteBase().replace(".", File.separator));
		caminho.append(File.separator);
		caminho.append(pacote);
		caminho.append(File.separator);
		caminho.append(artefato);
		
		return caminho.toString(); 
	}
	
	private String montarCaminhoXHTML(Entidade entidade, String artefato){
		StringBuilder caminho = new StringBuilder(entidade.getCaminho());
		caminho.append(File.separator); 
		caminho.append("webapp");
		caminho.append(File.separator);
		caminho.append("admin");
		caminho.append(File.separator);
		caminho.append(artefato);
		
		return caminho.toString(); 
	}
	
}
