package br.com.carrerasistemas.util;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Map;

import javax.servlet.ServletContext;

import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;

public class FreeMarkerUtil {
	
	private static Configuration cfg = new Configuration(Configuration.VERSION_2_3_22);

	 

    private static final String TEMPLATES_FOLDER = "freemarker";

   

    @SuppressWarnings("rawtypes")
	public static final String processarTemplate(Map map,String templateName, ServletContext context) throws TemplateException, IOException  {

          //diretório onde estão templates
          //cfg.setDirectoryForTemplateLoading(new File(TEMPLATES_FOLDER));
          cfg.setServletContextForTemplateLoading(context, TEMPLATES_FOLDER);
          cfg.setObjectWrapper(new DefaultObjectWrapper());
          cfg.setDefaultEncoding("UTF-8");
          cfg.setTemplateExceptionHandler(TemplateExceptionHandler.HTML_DEBUG_HANDLER);

          //recupera o template
          Template t = cfg.getTemplate(templateName);
          StringWriter writer = new StringWriter();
          /** Freemarker **/
          t.process(map, writer);
          writer.flush();
          writer.close();

          return writer.toString();

    }

}
