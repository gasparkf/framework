package br.com.carrerasistemas.mb;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.omnifaces.util.Messages;

import br.com.carrerasistemas.bo.GeradorCrudBO;
import br.com.carrerasistemas.entidade.Atributo;
import br.com.carrerasistemas.entidade.Entidade;

@Named
@ViewScoped
public class GeradorCrudMB implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@EJB
	private GeradorCrudBO bo;
	
	private Entidade entidade;
	
	private String nome;
	
	private String tipo;
	
	private String anotacao;
	
	private String validacao;

	@PostConstruct
	public void iniciar(){
		
		this.entidade = new Entidade();
	}
	
	public void gerarNomes(){
		getEntidade().setBo(getEntidade().getNome()+"BO");
		getEntidade().setMb(getEntidade().getNome()+"MB");
		getEntidade().setXhtml(getEntidade().getNome().toLowerCase());
	}
	
	public void addAtributo(){
		Atributo atributo = new Atributo();
		atributo.setNome(nome);
		atributo.setTipo(tipo);
		atributo.setAnotacao(anotacao);
		atributo.setValidacao(validacao);
		
		getEntidade().getAtributos().add(atributo);
	}
	
	public void removerAtributo(Atributo atributo){
		getEntidade().getAtributos().remove(atributo);
	}
	
	public void gerar(){
		try{
			bo.gerar(getEntidade());
			Messages.addGlobalInfo("CRUD gerado com sucesso!");
		}catch(Exception e){
			Messages.addGlobalError("Erro ao gerar o crud!");
		}
	}
	
	public Entidade getEntidade() {
		return entidade;
	}

	public void setEntidade(Entidade entidade) {
		this.entidade = entidade;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getAnotacao() {
		return anotacao;
	}

	public void setAnotacao(String anotacao) {
		this.anotacao = anotacao;
	}

	public String getValidacao() {
		return validacao;
	}

	public void setValidacao(String validacao) {
		this.validacao = validacao;
	}
	

}
