package ${entidade.pacoteBase}.bo;

import javax.ejb.Stateless;
import br.com.gaspar.framework.modelo.BaseBO;
import javax.ejb.EJB;
import ${entidade.pacoteBase}.dao.ProjetoDAO;
import ${entidade.pacoteBase}.entidade.${entidade.nome};


@Stateless
public class ${entidade.bo} extends BaseBO<${entidade.nome}>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@EJB
	private ProjetoDAO<${entidade.nome}> dao;
	
	@Override
	public ProjetoDAO<${entidade.nome}> getDAOPadrao() {
		return dao;
	}
}

