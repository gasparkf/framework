package ${entidade.pacoteBase}.entidade;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotNull;
import br.com.gaspar.utils.entidade.EntidadeBase;

@Entity
@NamedQueries({
	@NamedQuery(name="${entidade.nome}.buscarTodos", query="select obj from ${entidade.nome} obj order by obj.id")
})
public class ${entidade.nome} extends EntidadeBase{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(name = "${entidade.nome}", sequenceName = "${entidade.nome}_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "${entidade.nome}")
	private Long id;
	
	<#list entidade.atributos as at>
	${at.anotacao}
	private ${at.tipo} ${at.nome};
	</#list>
	
	public ${entidade.nome}() {
		super();
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		${entidade.nome} other = (${entidade.nome}) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}	
}

