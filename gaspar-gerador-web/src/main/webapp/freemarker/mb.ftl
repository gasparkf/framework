package ${entidade.pacoteBase}.mb;

import javax.faces.view.ViewScoped;
import javax.inject.Named;
import br.com.gaspar.framework.visao.jsf.ManagedBeanBase;
import javax.ejb.EJB;
import javax.annotation.PostConstruct;
import br.com.gaspar.utils.exception.BaseException;
import ${entidade.pacoteBase}.bo.${entidade.bo};
import ${entidade.pacoteBase}.entidade.${entidade.nome};

@Named
@ViewScoped
public class ${entidade.mb} extends ManagedBeanBase<${entidade.nome}>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@EJB
	private ${entidade.bo} bo;
	
	@Override
	protected ${entidade.bo} getFachada() {
		return bo;
	}
	
	@PostConstruct
	@Override
	public void iniciar() {
		//this.resultadoMaximo = 10;
		super.iniciar();
		setEntidade(new ${entidade.nome}());
	}
	
	@Override
	protected void criarAntes() throws BaseException {
		super.criarAntes();
		setEntidade(new ${entidade.nome}());
	}
}

