<?xml version='1.0' encoding='UTF-8' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<ui:composition xmlns="http://www.w3.org/1999/xhtml"
	xmlns:ui="http://xmlns.jcp.org/jsf/facelets"
	xmlns:h="http://xmlns.jcp.org/jsf/html"
	xmlns:f="http://xmlns.jcp.org/jsf/core"
	xmlns:fn="http://xmlns.jcp.org/jsp/functions"
	xmlns:c="http://xmlns.jcp.org/jsp/core"
	xmlns:jsf="http://xmlns.jcp.org/jsf"
	template="/WEB-INF/templates/crud.xhtml">

	<ui:param name="MBPadrao" value="${r"#{"}${entidade.mb?uncap_first}${r"}"}" />
	
	<ui:define name="titulo">
		Título
	</ui:define>
	
	<ui:define name="cabecalhoTabela">
		<#list entidade.atributos as at>
			<th>${at.nome?capitalize}</th>
		</#list>
		<th class="center">Ação</th>
	</ui:define>
	
	<ui:define name="conteudoTabela">
		<#list entidade.atributos as at>
		<td>${r"#{"}obj.${at.nome}${r"}"}</td>
		</#list>
	</ui:define>
	
	<ui:define name="formulario">
		<div class="row">
			<#list entidade.atributos as at>
			<div class="row">
				<div class="input-field col s12 m6">
					<input type="text" jsf:id="${at.nome}" jsf:value="${r"#{"}MB.entidade.${at.nome}${r"}"}" class="validate" jsf:maxlength="255" />
					<label for="${at.nome}">${at.nome?capitalize}</label>
				</div>
			</div>
			</#list>
		</div>
	</ui:define>
</ui:composition>