<?xml version='1.0' encoding='UTF-8' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<ui:composition xmlns="http://www.w3.org/1999/xhtml"
	xmlns:ui="http://java.sun.com/jsf/facelets"
	xmlns:h="http://java.sun.com/jsf/html"
	xmlns:f="http://java.sun.com/jsf/core"
	xmlns:p="http://primefaces.org/ui"
	xmlns:fn="http://java.sun.com/jsp/jstl/functions"
	template="/WEB-INF/facelets/admin.xhtml">

	
	<ui:define name="conteudo">
		<h:form styleClass="form-horizontal">
			<ui:param name="MB" value="${r"#{"}${entidade.mb}${r"}"}" />
			
			<p:messages id="messages" styleClass="text-danger" autoUpdate="true"/>
			<h:panelGroup rendered="${r"#{"}MB.modoListar${r"}"}">
				<div class="panel panel-default panel-primary">
					<div class="panel-heading">
						<h3>Cadastro de ${entidade.nome}</h3> 
					</div>
					<div class="panel-body">
						
							<div class="well">
								<div class="form-group">
									<label for="nomePesquisa" class="col-md-1 control-label">Nome</label>
									<div class="col-md-3">
										<h:inputText id="nomePesquisa" value="${r"#{"}MB.nomePesquisa${r"}"}" styleClass="form-control" maxlength="50"/>
									</div>
									<div class="col-md-1">
										<h:commandLink action="${r"#{"}MB.pesquisar${r"}"}" styleClass="btn btn-info">
											<span class="glyphicon glyphicon-search"></span> Pesquisar
											<f:ajax execute="@form" render="@form"></f:ajax>
										</h:commandLink>
									</div>
								</div>
								
							</div>
						
						<div class="row" style="padding-left: 10px; padding-right: 10px;">
							<h:commandLink action="${r"#{"}MB.criar${r"}"}" styleClass="btn btn-success">
								<span class="glyphicon glyphicon-plus"></span> Novo
							</h:commandLink>
													
							<div class="pull-right">
								<h4>
									<span class="label label-primary">Total de registros: <h:outputText value="${r"#{"}MB.totalRegistros${r"}"}"/></span>
								</h4>
							</div>
						</div>
					</div>
					<hr></hr>
					<ui:decorate template="/WEB-INF/templates/paginador.xhtml"></ui:decorate>
					
					<table class="table table-hover table-striped">
						<thead>
							<tr>
								<th>#</th>
								<#list entidade.atributos as at>
								<th>${at.nome}</th>
								</#list>
								<th>Ação</th>
							</tr>
						</thead>
						<tbody>
							<h:panelGroup rendered="${r"#{"}fn:length(MB.lista) > 0${r"}"}">
								<ui:repeat var="obj" value="${r"#{"}MB.lista${r"}"}" varStatus="i">
									<tr>
										<td><h:outputText value="${r"#{"}i.index + 1${r"}"}" /></td>
										<#list entidade.atributos as at>
										<td>${r"#{"}obj.${at.nome}${r"}"}</td>
										</#list>
											
										<td>
											<h:commandLink action="${r"#{"}MB.editar(obj)${r"}"}" styleClass="btn btn-warning">
												<span class="glyphicon glyphicon-folder-open"></span>&nbsp;Editar 
											</h:commandLink>
										</td>
									</tr>
								</ui:repeat>
							</h:panelGroup>		
						</tbody>  
					</table>
				</div>
			</h:panelGroup>
			
			<h:panelGroup rendered="${r"#{"}MB.modoEditar${r"}"}">
				<div class="panel panel-default panel-primary">
					<div class="panel-heading"><h3>Cadastro de ${entidade.nome}</h3></div>
						
					<div class="panel-body">
						<#list entidade.atributos as at>
				   		<div class="form-group">
					 		<label for="${at.nome}" class="col-md-1 control-label">${at.nome}</label>
					 		<div class="col-md-11">
								<h:inputText id="${at.nome}" value="${r"#{"}MB.entidade.${at.nome}${r"}"}" styleClass="form-control" maxlength="255"/>
							</div>
						</div>
						</#list>
						<hr />
					</div>
				 	
				 	<div class="panel-footer">
						<div class="container">
					 		<div class="form-group">
					 			<div class="col-md-2">					    	
									<h:commandLink styleClass="btn btn-primary" action="${r"#{"}MB.voltar${r"}"}" immediate="true">
										<span class="glyphicon glyphicon-arrow-left"></span> Voltar
									</h:commandLink>
								</div>
								&nbsp;	
								<div class="col-md-2">
								  	<h:commandLink styleClass="btn btn-success" action="${r"#{"}MB.gravar${r"}"}">
										<span class="glyphicon glyphicon-floppy-disk"></span> Gravar
									</h:commandLink>
								</div>
								&nbsp;
								<div class="col-md-2">
									<h:commandLink styleClass="btn btn-danger" action="${r"#{"}MB.excluir${r"}"}" immediate="true" onclick="return confirm('Confirma exclusão do item?')" rendered="${r"#{"}MB.entidade.id != null${r"}"}">
										<span class="glyphicon glyphicon-trash"></span> Excluir
									</h:commandLink>
								</div>
							</div>
						</div>
					</div>
				</div>
			</h:panelGroup>
		</h:form>
	</ui:define>
</ui:composition>