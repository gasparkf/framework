package br.com.gaspar.utils;

import javax.swing.JOptionPane;

/**
 * Classe utilitária para mostrar mensagens com JOptionPane
 * 
 * @author gaspar
 *
 */
public class JOptionPaneUtil {
	
	/**
	 * Mostra Mensagem padrão com ícone de warning
	 * @param mensagem
	 * @param titulo
	 * 
	 * @author gaspar
	 */
	public static void mostrarMensagemAviso(String mensagem, String titulo){
		JOptionPane.showMessageDialog(null, mensagem, titulo, JOptionPane.WARNING_MESSAGE);
	}
	
	
	/**
	 * Mostra Mensagem padrão com ícone de warning e título de "Aviso"
	 * @param mensagem
	 *  
	 * @author gaspar
	 */
	public static void mostrarMensagemAviso(String mensagem){
		JOptionPane.showMessageDialog(null, mensagem, "Aviso", JOptionPane.WARNING_MESSAGE);
	}
	
	/**
	 * Mostra Mensagem padrão com ícone de informação e título de "Aviso"
	 * @param mensagem
	 * 
	 * @author gaspar
	 */
	public static void mostrarMensagemInformacao(String mensagem){
		JOptionPane.showMessageDialog(null, mensagem, "Aviso", JOptionPane.INFORMATION_MESSAGE);
	}
	
	/**
	 * Mostra Mensagem padrão com ícone de aviso
	 * 
	 * @param mensagem
	 * @param titulo
	 * 
	 * @author gaspar
	 */
	public static void mostrarMensagemInformacao(String mensagem, String titulo){
		JOptionPane.showMessageDialog(null, mensagem, titulo, JOptionPane.INFORMATION_MESSAGE);
	}
	
	/**
	 * Mostra Mensagem padrão com ícone de informação e título de "Aviso"
	 * @param mensagem
	 * 
	 * @author gaspar
	 */
	public static void mostrarMensagemErro(String mensagem){
		JOptionPane.showMessageDialog(null, mensagem, "Erro", JOptionPane.ERROR_MESSAGE);
	}
	
	/**
	 * Mostra Mensagem padrão com ícone de aviso
	 * 
	 * @param mensagem
	 * @param titulo
	 * 
	 * @author gaspar
	 */
	public static void mostrarMensagemErro(String mensagem, String titulo){
		JOptionPane.showMessageDialog(null, mensagem, titulo, JOptionPane.ERROR_MESSAGE);
	}

}
