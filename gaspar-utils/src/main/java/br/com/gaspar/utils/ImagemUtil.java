package br.com.gaspar.utils;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.Serializable;

import javax.imageio.ImageIO;

import org.imgscalr.Scalr;
import org.imgscalr.Scalr.Method;

/**
 * Classe utilitária para tratamento de imagens
 * @author Gaspar
 *
 */
public class ImagemUtil implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Reduz o tamanho da imagem mantendo a proporção
	 * @param imagem
	 * @param tamanhoHorizontal
	 */
	public static void reduzirImagem(File imagem, Integer tamanhoHorizontal){
		 	try {
	            BufferedImage bufimage = ImageIO.read(imagem);

	            BufferedImage bISmallImage = Scalr.resize(bufimage, tamanhoHorizontal); // after this line my dimensions in bISmallImage are correct!
	            //bISmallImage = Scalr.pad(bISmallImage, 4);
	            ImageIO.write(bISmallImage, "jpg", imagem); // but my smallImage has the same dimension as the original foto
	        } catch (Exception e) {
	            System.out.println(e.getMessage()); // FORNOW: added just to be sure
	        }
	}
	
	/**
	 * Reduz o tamanho da imagem mantendo a proporção e faz um tratamento para imagem ficar mais suave e com mais brilho
	 * @param imagem
	 * @param tamanhoHorizontal
	 */
	public static void reduzirImagemTratamento(File imagem, Integer tamanhoHorizontal){
	 	try {
            BufferedImage bufimage = ImageIO.read(imagem);

            BufferedImage bISmallImage = Scalr.resize(bufimage, Method.SPEED, tamanhoHorizontal, Scalr.OP_ANTIALIAS, Scalr.OP_BRIGHTER);
            //bISmallImage = Scalr.pad(bISmallImage, 4);
            ImageIO.write(bISmallImage, "jpg", imagem); // but my smallImage has the same dimension as the original foto
        } catch (Exception e) {
            System.out.println(e.getMessage()); // FORNOW: added just to be sure
        }
}

}
