/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.gaspar.utils;

/**
 *
 * @author iortiz
 */
public class NativeSQLQuery {

    public static final String SQL_CONSULTAR_CADASTRO_CONTRIBUINTE =
        "SELECT a.CON_INSC_EST As ie, a.CON_CNPJ_CPF As cnpj, a.CON_CNPJ_CPF " +
        "As cpf, b.UFE_CODIGO As siglaUF, a.CON_SITUACAO As situacaoCadastral, " +
        "a.CON_NOME As xNome, a.CON_NOME_FANTASIA As xNomeFantasia, " +
        "a.CON_REGIME_PGTO As xRegimeICMS, a.CNAE_CODIGO As cnae, " +
        "a.CON_DT_INICIO_ATV As dataInicioAtividades, a.CON_DT_ATUALIZACAO " +
        "As dataUltimaModificacao, a.CON_DT_ATUALIZACAO As dataBaixa," +
        "a.CON_INSC_EST As ieUnica, a.CON_INSC_EST As ieAtual, a.CON_ENDERECO " +
        "As logradouro, a.CON_NUM_ENDERECO As numero, a.CON_COMPLEMENTO " +
        "As complemento, a.CON_BAIRRO As bairro, a.MUN_CODIGO_PROP As cMun, " +
        "b.MUN_NOME As xMun, a.CON_CEP As cep, a.CON_TP_PESSOA As tipoPessoa " +
        "FROM CONTRIBUINTE a " +
        "INNER JOIN MUNICIPIO b on(a.MUN_CODIGO_PROP = b.MUN_CODIGO) ";
    
    public static final String SQL_WHERE_CONSULTA_CADASTRO_IE =
        "WHERE a.CON_INSC_EST = :p0";

    public static final String SQL_WHERE_CONSULTA_CADASTRO_CNPJ_CPF =
        "WHERE a.CON_CNPJ_CPF = :p0";

    public static final String SQL_SELECT_TEMPO_MEDIO_PROCESSAMENTO =
        "SELECT   SUM(quantidadeSegundoProcessamento) AS totalSegundos, " +
        "         COUNT(quantidadeDocumento) AS totalLotes, " +
        "         SUM(quantidadeDocumento) AS totalNotas " +
        "  FROM   nfe_estatistica_processamento  " +
        " WHERE   dataProcessamento >= DateAdd(mi, -5, GETDATE()) ";

    public static final String SQL_SELECT_PESSOA_JURIDICA =
        "SELECT P.* FROM nfe_pessoa_juridica P " +
        "WHERE P.cnpjEmitente = :p0 ";

    public static final String SQL_CONSULTAR_CADASTRO_CTE_CREDENCIAMENTO =
        "SELECT a.CNPJ As cnpj, a.IE As ie, a.Empresa as empresa, " +
        "a.ativo As ativo " +
        "FROM ContribuintesHabilitados a ";

    public static final String SQL_WHERE_CONSULTA_CADASTRO_CTE_IE =
        "WHERE a.IE = :p0";

    public static final String SQL_WHERE_CONSULTA_CADASTRO_IE_CNPJ_PESSOA_JURIDICA =
        "WHERE a.CON_INSC_EST = :p0 and a.CON_CNPJ_CPF = :p1 and a.CON_TP_PESSOA = 'J' ";

    public static final String SQL_WHERE_CONSULTA_CADASTRO_IE_PESSOA_JURIDICA =
        "WHERE a.CON_INSC_EST = :p0 and a.CON_TP_PESSOA = 'J' ";

     public static final String SQL_WHERE_CONSULTA_CADASTRO_CNPJ_PESSOA_JURIDICA =
        "WHERE a.CON_CNPJ_CPF = :p0 and a.CON_TP_PESSOA = 'J' ";

     // acrescentado o top 1, retirou o ano da consulta e pode retornar uma lista
     public static final String SQL_CONSULTAR_CHAVE_DUPLICADA =
        "SELECT top(1) * " +
        "FROM nfe_nota_uf_local a " +
        "INNER JOIN nfe_pessoa_juridica b on(a.idPjr = b.idPjr) " +
        "WHERE b.cnpjEmitente = :p0 " +
        "and a.modelo = :p1 and a.serie = :p2 and a.numeroDocumentoFiscal = :p3 " +
        "order by a.dataFimProcessamento desc ";
     
     /**
      * SQL omitizado
      * Retorna apenas o id e a dataEmissao
      * acrescentado o top 1, retirou o ano da consulta e pode retornar uma lista
      */
     public static final String SQL_DATA_EMISSAO_CONSULTAR_CHAVE_DUPLICADA =
    	        "SELECT top(1) a.idNul as idNul, a.dataEmissao as dataEmissao FROM nfe_nota_uf_local a " +
    	        "INNER JOIN nfe_pessoa_juridica b on(a.idPjr = b.idPjr) " +
    	        "WHERE b.cnpjEmitente = :p0 " +
    	        "and a.modelo = :p1 and a.serie = :p2 and a.numeroDocumentoFiscal = :p3 " + 
                "order by a.dataFimProcessamento desc ";
}
