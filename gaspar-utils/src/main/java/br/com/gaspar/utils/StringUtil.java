package br.com.gaspar.utils;


/**
 * 
 * @author gaspar
 *
 */
public class StringUtil {
	
    
    /**
     * Remove os espa�os em branco a direita de uma string
     */
    public static String rtrim(String str) throws Exception {
        char chr[] = str.toCharArray();
        int lastIndexSpaceChar = 0;
        for(int i = str.length()-1; i >= 0; i--) {
            if (!Character.isSpaceChar(chr[i])) {
                lastIndexSpaceChar = (i+1);
                break;
            }
        }
        return str.substring(0, lastIndexSpaceChar);
    }
    
    /**
	 * Recebe uma String e a normaliza para o padrão java
	 * Primeira letra em minúscula
	 * @param texto
	 * @return
	 * @author gaspar
	 */
	public static String normalizar(String texto){
		StringBuilder sb = new StringBuilder(texto);
		texto = null;
		if(sb.length() > 1){
			sb.replace(0, 1, sb.substring(0, 1).toLowerCase());
		}
		texto = sb.toString();
		sb = null;
		return texto;
	}
	
	/**
	 * Testa se a string está nula ou vazia
	 * @param texto
	 * @return
	 * 
	 * @author gaspar
	 */
	public static Boolean ehBrancoOuNulo(String texto){
		Boolean ret = Boolean.TRUE;
		if(texto != null && texto.trim().length() > 0){
			ret = Boolean.FALSE;
		}
		return ret;
	}
	
	/**
	 * 
	 * @param mac
	 * @return
	 * 
	 * @author gaspar
	 */
	public static String converterMACByteArray(byte[] mac){
        StringBuilder strBuilder = new StringBuilder();
        
        for (int i = 0; i < mac.length; i++) {
            byte b = mac[i];
            String hexadecimal = "0"+Integer.toHexString(b);
            strBuilder.append(hexadecimal.substring(hexadecimal.length() - 2)).append(i < mac.length - 1 ? "-" : "");
        }

        return strBuilder.toString();
    }

}
