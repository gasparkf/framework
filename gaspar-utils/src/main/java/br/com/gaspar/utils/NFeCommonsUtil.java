/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.gaspar.utils;

import java.io.Serializable;

/*import gov.sefaz.nfe.commons.entidade.constantes.ConstantsService;
import gov.sefaz.nfe.commons.entidade.xml.InfoMovimentacaoEnvioSRFXML;
import gov.sefaz.utils.annotations.NFeServiceType;
import gov.sefaz.utils.annotations.XMLElementoNS;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Set;
import javax.persistence.Entity;
import java.io.Serializable;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.text.MessageFormat;
import javax.security.auth.Subject;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;*/

/**
 *
 * @author chsilva
 */
public class NFeCommonsUtil implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;/*

    *//**
	 * 
	 *//*
	private static final long serialVersionUID = -7042757976731378215L;
	private static final String PROVIDER = "SUN";
    private static CertificateFactory certFactory;
    private static Logger log = Logger.getLogger(NFeCommonsUtil.class);

    static{
        try{
            certFactory = CertificateFactory.getInstance("X.509", PROVIDER);
        }catch (Exception ex){
           log.error("Erro na inicializacao da estrutura de Certificado" + PROVIDER + ex);
        }
    }

    public static byte[] getArquivoCompactado(String xml) {

        try {
            return ArquivoUtil.zip(xml.getBytes("UTF-8"), 9, "arquivo.xml");
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public static String getArquivoDescompactado(byte[] xml) {

        try {
            return ArquivoUtil.unzipString(xml);
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
    }
    
        *//**
     *
     *
     * @param bean
     * @return
     *//*
    public static String getServiceType(Object bean) {

        NFeServiceType st = bean.getClass().getAnnotation(NFeServiceType.class);

        if (st == null) return null;
        if (st.service() == null || st.service().trim().length() == 0) return null;

        return st.service();

    }

        *//**
     *
     *
     * @param bean
     * @return
     *//*
    public static Boolean getEntity(Object bean) {

        if (bean.getClass().getAnnotation(Entity.class) == null){
            return false;
        }
        else{
            return true;
        }
    }



    *//**
     * Retorna o primeiro certificado do array de certificados associados ao
     * request, ou null se nao houver certificados.
     * @param request
     * @return
     *//*
    public synchronized static X509Certificate getCertificado(HttpServletRequest request) {
        X509Certificate[] certificados = getCertificados(request);
        if (certificados == null || certificados.length == 0) {
            return null;
        }
        return certificados[0];
    }

    public synchronized static X509Certificate getCertificado(Subject subject) {
        AmpulhetaUtil cronometro = new AmpulhetaUtil("getCertificado");
        cronometro.iniciar();

        Set<Object> certificados = subject.getPublicCredentials();
        if (certificados == null || certificados.size() == 0) {
            return null;
        }

        log.info(cronometro.parar());
        
        return (X509Certificate) (certificados.toArray())[0];
    }

    *//**
     * Retorna o array de certificados associados ao request, ou null se nao
     * houver certificados.
     * @param request
     * @return
     *//*
    public synchronized static X509Certificate[] getCertificados(HttpServletRequest request) {
        return (X509Certificate[]) request.getAttribute("javax.servlet.request.X509Certificate");
    }

    *//**
     *
     * @param certFile
     * @return
     * @throws java.io.IOException
     * @throws java.security.cert.CertificateException
     *//*
    public synchronized static X509Certificate getCertificado(File certFile)
            throws IOException, CertificateException {
        InputStream is = new FileInputStream(certFile);
        X509Certificate cert = getCertificado(is);
        is.close();
        return cert;
    }

    *//**
     *
     * @param is
     * @return
     * @throws java.security.cert.CertificateException
     *//*
    public synchronized static X509Certificate getCertificado(InputStream is)
            throws CertificateException {
        return (X509Certificate) certFactory.generateCertificate(is);
    }

    *//**
     *
     * @param xml
     * @return
     *//*
    public static String getNFeProcType(Document xml) {

        String nfeProcType  = xml.getElementsByTagName(ConstantsService.PROC_NFE)
                .item(0).getNodeValue();

        if (nfeProcType != null && nfeProcType.trim().length() > 0) {
            return ConstantsService.PROC_NFE;
        }

        nfeProcType  = xml.getElementsByTagName(ConstantsService.PROC_CANC_NFE)
                .item(0).getNodeValue();

        if (nfeProcType != null && nfeProcType.trim().length() > 0) {
            return ConstantsService.PROC_CANC_NFE;
        }

        nfeProcType  = xml.getElementsByTagName(ConstantsService.PROC_INUT_NFE)
                .item(0).getNodeValue();

        if (nfeProcType != null && nfeProcType.trim().length() > 0) {
            return ConstantsService.PROC_INUT_NFE;
        }

        return null;
    }

    public static File getFileProcXml(InfoMovimentacaoEnvioSRFXML movimento,
                                      char tipo, String versao  ) {

        File fileProcXml = null;
        String nomeArquivo = null;

        try {

            nomeArquivo = "/nfelog/tmpnfe/" +
                getNameFileProcXml(movimento.getProtocolo(),versao, tipo);
            fileProcXml = new File(nomeArquivo);
            ArquivoUtil.salvarArquivo(fileProcXml,
                NFeCommonsUtil.getArquivoDescompactado(movimento.getProcXml()));

            log.debug("Arquivo procXml criado: " + fileProcXml.getAbsolutePath() +
                    " - existe? " + fileProcXml.exists());

            return fileProcXml;

        } catch (Exception ex) {
            log.error("Ocorreu um erro ao gerar o arquivo procXml", ex);
        }

        return null;
    }

    *//**
     *
     * @param protocolo
     * @param versao
     * @return
     *//*
    public static String getNameFileProcXml(Long protocolo, String versao, char tipo) {

        Object[] params = {
            String.valueOf(protocolo), versao
        };

        if (tipo == ConstantsService.TIPO_NFE_MOVIMENTO_AUTORIZADA) {
            return MessageFormat.format(
                ConstantsService.PATTERN_NOME_ARQUIVO_AUTORIZADA, params)+ ".xml";
        }

        if (tipo == ConstantsService.TIPO_NFE_MOVIMENTO_CANCELADA) {
            return MessageFormat.format(
                ConstantsService.PATTERN_NOME_ARQUIVO_CANCELADA, params)+ ".xml";
        }

        if (tipo == ConstantsService.TIPO_NFE_MOVIMENTO_INUTILIZADA) {
            return MessageFormat.format(
                ConstantsService.PATTERN_NOME_ARQUIVO_INUTILIZADA, params)+ ".xml";

        }

        return null;
    }

    *//**
     * Retorna os elementos do XML, que deverao ser validados o namespace.
     * @param bean
     * @param versao
     * @return
     *//*
    public static String[] xmlElementoNS(Object bean, String versao) {
        NFeServiceType st = bean.getClass().getAnnotation(NFeServiceType.class);
        if (st.elementoNS() == null || st.elementoNS().length == 0) {
            return null;
        }
        for (int i = 0; i < st.elementoNS().length; i++) {
            XMLElementoNS esq = st.elementoNS()[i];
            if (esq.versao().equals(versao)) {
                return esq.elemento();
            }
        }
        if (st.elementoNS().length > 0) {
            // caso não tenha encontrado para versão solicitada, retorna a
            // ultima versão vigente
            return st.elementoNS()[0].elemento();
        }
        return null;
    }
    *//**
     *
     * @param bean
     * @return
     *//*
    public static String xmlNamespace(Object bean) {
        NFeServiceType st = bean.getClass().getAnnotation(NFeServiceType.class);
        if (st.nomeNamespaceXML() == null || st.nomeNamespaceXML().trim().length() == 0) {
            return null;
        }
        return st.nomeNamespaceXML();
    }*/
}
