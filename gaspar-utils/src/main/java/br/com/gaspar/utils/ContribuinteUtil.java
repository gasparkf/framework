package br.com.gaspar.utils;

/**
 *
 * @author gmnascimento
 */
public class ContribuinteUtil {

    /**
     * Metodo que retorna o tipo de contribuiente (CCI ou CAP) de acordo
     * com o terceiro digito da inscricao estadual emitida pelo MS
     * @param inscricaoEstadual 
     * @return Tipo Contribuinte
     */
    public static String getTipoContribuinte(String inscricaoEstadual) {
        if (ehContribuinteDoMS(inscricaoEstadual)) {

            int tipoContribuinte = Integer.valueOf(inscricaoEstadual.substring(2, 3));

            if (tipoContribuinte < 5) {
                return "CCI";
            } else {
                return "CAP";
            }
        } else {
            return null;
        }
    }
    /**
     * Metodo que retorna se a inscricao estadual do contribuiente eh do MS
     * comeca com 28 seguido de 7 digitos numericos
     * @param inscricaoEstadual 
     * @return Boolean
     */
    public static Boolean ehContribuinteDoMS(String inscricaoEstadual) {
        if (inscricaoEstadual.matches("^28[0-9]{7}$")) {
            return Boolean.TRUE;
        } else {
            return Boolean.FALSE;
        }
    }
}
