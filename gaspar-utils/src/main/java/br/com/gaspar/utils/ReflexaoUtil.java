package br.com.gaspar.utils;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * 
 * @author gaspar
 *
 */
public class ReflexaoUtil {
	
	/**
	 * Retorna o objeto Method referente ao nome do método do objeto e seus parâmetros caso existam
	 * @param objeto
	 * @param nomeMetodo
	 * @param parametros
	 * @return
	 * 
	 * @author gaspar
	 */
	public static Method getMetodo(Object objeto, String nomeMetodo, Class<?> ...parametros){
		Method objMetodo = null;
		try {
			objMetodo = objeto.getClass().getMethod(nomeMetodo, parametros);
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} 
		return objMetodo;
	}
	
	/**
	 * Executa o método passado no objeto junto com seus parâmetros caso existam
	 * @param metodo
	 * @param objeto
	 * @param parametros
	 * @return
	 * 
	 * @author gaspar
	 * 
	 */
	public static Object executaMetodo(Method metodo, Object objeto, Object ...parametros){
		Object retorno = null;
		try {
			retorno = metodo.invoke(objeto, parametros);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
		return retorno;
	}

}
