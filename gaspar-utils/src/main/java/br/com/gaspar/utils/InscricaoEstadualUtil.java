/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.gaspar.utils;


/**
 *
 * @author tmichelini
 */
public class InscricaoEstadualUtil {

    //private static Logger log = Logger.getLogger(InscricaoEstadualUtil.class);

    /**
     * 
     * @param ie
     * @param uf
     * @return
     */
   /* public static boolean isValid(String ie, String uf) {
        try {
            if (Long.valueOf(ie.trim()).equals(Long.valueOf("0"))) {
                return false;
            }

        } catch (Exception e) {
            log.error(e);
            return false;
        }
        return genericValid(ie, uf);
    }*/

    /**
     * Realiza validacao da IE de cada estado.
     * Obs: IE deve ser passada sem mascara.
     */
    /*private static boolean genericValid(String ie, String uf) {

        try {
            if (uf.equalsIgnoreCase("AC")) {
                new IEAcreValidator(false).assertValid(ie);
            } else if (uf.equalsIgnoreCase("AL")) {
                new IEAlagoasValidator(false).assertValid(ie);
            } else if (uf.equalsIgnoreCase("AP")) {
                new IEAmapaValidator(false).assertValid(ie);
            } else if (uf.equalsIgnoreCase("AM")) {
                new IEAmazonasValidator(false).assertValid(ie);
            } else if (uf.equalsIgnoreCase("BA")) {
                new IEBahiaValidator(false).assertValid(ie);
            } else if (uf.equalsIgnoreCase("CE")) {
                new IECearaValidator(false).assertValid(ie);
            } else if (uf.equalsIgnoreCase("DF")) {
                new IEDistritoFederalValidator(false).assertValid(ie);
            } else if (uf.equalsIgnoreCase("ES")) {
                new IEEspiritoSantoValidator(false).assertValid(ie);
            } else if (uf.equalsIgnoreCase("GO")) {
                new IEGoiasValidator(false).assertValid(ie);
            } else if (uf.equalsIgnoreCase("MA")) {
                new IEMaranhaoValidator(false).assertValid(ie);
            } else if (uf.equalsIgnoreCase("MT")) {
                new IEMatoGrossoValidator(false).assertValid(ie);
            } else if (uf.equalsIgnoreCase("MS")) {
                new IEMatoGrossoDoSulValidator(false).assertValid(ie);
            } else if (uf.equalsIgnoreCase("MG")) {
                new IEMinasGeraisValidator(false).assertValid(ie);
            } else if (uf.equalsIgnoreCase("PA")) {
                new IEParaValidator(false).assertValid(ie);
            } else if (uf.equalsIgnoreCase("PB")) {
                new IEParaibaValidator(false).assertValid(ie);
            } else if (uf.equalsIgnoreCase("PR")) {
                new IEParanaValidator(false).assertValid(ie);
            } else if (uf.equalsIgnoreCase("PE")) {
                new IEPernambucoValidator(false).assertValid(ie);
            } else if (uf.equalsIgnoreCase("PI")) {
                new IEPiauiValidator(false).assertValid(ie);
            } else if (uf.equalsIgnoreCase("RJ")) {
                new IERioDeJaneiroValidator(false).assertValid(ie);
            } else if (uf.equalsIgnoreCase("RN")) {
                new IERioGrandeDoNorteValidator(false).assertValid(ie);
            } else if (uf.equalsIgnoreCase("RS")) {
                new IERioGrandeDoSulValidator(false).assertValid(ie);
            } else if (uf.equalsIgnoreCase("RO")) {
                new IERondoniaValidator(false).assertValid(ie);
            } else if (uf.equalsIgnoreCase("RR")) {
                new IERoraimaValidator(false).assertValid(ie);
            } else if (uf.equalsIgnoreCase("SC")) {
                new IESantaCatarinaValidator(false).assertValid(ie);
            } else if (uf.equalsIgnoreCase("SP")) {
                new IESaoPauloValidator(false).assertValid(ie);
            } else if (uf.equalsIgnoreCase("SE")) {
                new IESergipeValidator(false).assertValid(ie);
            } else if (uf.equalsIgnoreCase("TO")) {
                new IETocantinsValidator(false).assertValid(ie);
            }
            return true;
        } catch (Exception e) {
            System.err.println(e.getMessage());
            return false;
        }

    }*/
}
