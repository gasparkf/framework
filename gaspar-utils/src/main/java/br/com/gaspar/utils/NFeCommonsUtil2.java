/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.gaspar.utils;

import java.io.Serializable;

/*import gov.sefaz.nfe.commons.entidade.constantes.ConstantsService;
import gov.sefaz.nfe.commons.entidade.xml.InfoMovimentacaoEnvioSRFXML;
import gov.sefaz.utils.annotations.NFeServiceType;
import gov.sefaz.utils.annotations.XMLElementoNS;

import java.io.File;
import java.io.IOException;
import java.util.Set;
import javax.persistence.Entity;
import java.io.Serializable;
import java.security.cert.X509Certificate;
import java.text.MessageFormat;
import javax.security.auth.Subject;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;*/

/**
 * Classe utilitária da NFe.
 * @author icorrea
 */
public class NFeCommonsUtil2 implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3713005185405384003L;/*

    *//**
	 * 
	 *//*
	private static final long serialVersionUID = -3526327902487097195L;
	*//**
     * variavel de log.
     *//*
    private static final Logger LOG = Logger.getLogger(NFeCommonsUtil2.class);

    *//**
     * Retorna um byte array da string.
     * @param xml : string xml a ser convertida para byte array.
     * @return : retorna o byte array da string xml.
     *//*
    public final byte[] getArquivoCompactado(final String xml) {

        try {
            return ArquivoUtil.zip(xml.getBytes("UTF-8"), 9, "arquivo.xml");
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    *//**
     * Converte byte array em string.
     * @param xml : byte array a ser convertido.
     * @return : retorna a string obtida a partir do byte array.
     *//*
    public final String getArquivoDescompactado(final byte[] xml) {

        try {
            return ArquivoUtil.unzipString(xml);
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    *//**
     * Retorna o tipo de serviço que o bean representa.
     * @param bean : objeto do serviço.
     * @return : tipo de serviço web que o objeto representa.
     *//*
    public final String getServiceType(final Object bean) {

        final NFeServiceType serviceType =
                bean.getClass().getAnnotation(NFeServiceType.class);

        return serviceType.service();

    }

    *//**
     * Retorna se o objeto é uma entidade JPA.
     * @param bean : objeto a ser testado.
     * @return : retorna true se for uma entidade JPA, senão retorna falso.
     *//*
    public final Boolean getEntity(final Object bean) {
        boolean retorno = true;
        if (bean.getClass().getAnnotation(Entity.class) == null) {
            retorno =  false;
        }
        return retorno;
    }



    *//**
     * Retorna o primeiro certificado do array de certificados associados ao
     * request, ou null se nao houver certificados.
     * @param request : requisição http.
     * @return : retorna o certificado da requisição http.
     *//*
    public final X509Certificate
            getCertificado(final HttpServletRequest request) {
        final X509Certificate[] certificados = getCertificados(request);
        if (certificados == null || certificados.length == 0) {
            return null;
        }
        return certificados[0];
    }

    *//**
     * Retorna o certificado a partir do subject.
     * @param subject : subject que possui o certificado.
     * @return : retorna o certificado digital.
     *//*
    public final X509Certificate getCertificado(final Subject subject) {
        final AmpulhetaUtil cronometro = new AmpulhetaUtil("getCertificado");
        cronometro.iniciar();
        final Set<Object> certificados = subject.getPublicCredentials();
        if (certificados == null || certificados.isEmpty()) {
            return null;
        }
        LOG.info(cronometro.parar());
        return (X509Certificate) (certificados.toArray())[0];
    }

    *//**
     * Retorna o array de certificados associados ao request, ou null se nao
     * houver certificados.
     * @param request : requisição http.
     * @return : retorna o certificado digital da requisição http.
     *//*
    public final X509Certificate[]
            getCertificados(final HttpServletRequest request) {
        return (X509Certificate[])
                request.getAttribute("javax.servlet.request.X509Certificate");
    }


    *//**
     * Retorna o NFeProc do documento XML.
     * @param xml : documento XML.
     * @return : retorna o NFeProc.
     *//*
    public final String getNFeProcType(final Document xml) {

        String nfeProcType  =
                xml.getElementsByTagName(ConstantsService.PROC_NFE)
                .item(0).getNodeValue();

        if (nfeProcType != null && !nfeProcType.isEmpty()) {
            return ConstantsService.PROC_NFE;
        }

        nfeProcType  = xml.getElementsByTagName(ConstantsService.PROC_CANC_NFE)
                .item(0).getNodeValue();

        if (nfeProcType != null && !nfeProcType.isEmpty()) {
            return ConstantsService.PROC_CANC_NFE;
        }

        nfeProcType  = xml.getElementsByTagName(ConstantsService.PROC_INUT_NFE)
                .item(0).getNodeValue();

        if (nfeProcType != null && !nfeProcType.isEmpty()) {
            return ConstantsService.PROC_INUT_NFE;
        }

        return null;
    }

    *//**
     * Retorna o arquivo do XML do movimento.
     * @param movimento : Objeto com o XML a ser movimentado.
     * @param tipo : tipo de movimento.
     * @param versao : versão do XML.
     * @return : retorna o arquivo XML.
     *//*
    public final File
            getFileProcXml(final InfoMovimentacaoEnvioSRFXML movimento,
                           final char tipo,
                           final String versao) {
        File fileProcXml = null;
        String nomeArquivo = null;

        try {

            nomeArquivo = "/nfelog/tmpnfe/"
                   + getNameFileProcXml(movimento.getProtocolo(), versao, tipo);
            fileProcXml = new File(nomeArquivo);
            ArquivoUtil.salvarArquivo(fileProcXml,
                getArquivoDescompactado(movimento.getProcXml()));

            LOG.debug("Arquivo procXml criado: "
                    + fileProcXml.getAbsolutePath()
                    + " - existe? " + fileProcXml.exists());

            return fileProcXml;

        } catch (Exception ex) {
            LOG.error("Ocorreu um erro ao gerar o arquivo procXml", ex);
        }

        return null;
    }

    *//**
     * Gera o nome do arquivo PROCXML.
     * @param protocolo : número do protocolo.
     * @param versao : versão do arquivo XML.
     * @param tipo : tipo de autorização.
     * @return : retorna o nome do arquivo procxml.
     *//*
    public final String getNameFileProcXml(final Long protocolo,
                                           final String versao,
                                           final char tipo) {
        final Object[] params = {String.valueOf(protocolo), versao};

        if (tipo == ConstantsService.TIPO_NFE_MOVIMENTO_AUTORIZADA) {
            return MessageFormat.format(
             ConstantsService.PATTERN_NOME_ARQUIVO_AUTORIZADA, params) + ".xml";
        }

        if (tipo == ConstantsService.TIPO_NFE_MOVIMENTO_CANCELADA) {
            return MessageFormat.format(
              ConstantsService.PATTERN_NOME_ARQUIVO_CANCELADA, params) + ".xml";
        }

        if (tipo == ConstantsService.TIPO_NFE_MOVIMENTO_INUTILIZADA) {
            return MessageFormat.format(
            ConstantsService.PATTERN_NOME_ARQUIVO_INUTILIZADA, params) + ".xml";

        }

        return null;
    }

    *//**
     * Retorna os elementos do XML, que deverao ser validados o namespace.
     * @param bean : bean de serviço.
     * @param versao : versão do XML.
     * @return : retorna os nomes do elementos que deverão validados o
     *           namespace.
     *//*
    public final String[] xmlElementoNS(final Object bean,
                                        final String versao) {
        final NFeServiceType serviceType =
                bean.getClass().getAnnotation(NFeServiceType.class);
        if (serviceType.elementoNS() == null
                || serviceType.elementoNS().length == 0) {
            return null;
        }
        for (int i = 0; i < serviceType.elementoNS().length; i++) {
            final XMLElementoNS esq = serviceType.elementoNS()[i];
            if (esq.versao().equals(versao)) {
                return esq.elemento();
            }
        }
        if (serviceType.elementoNS().length > 0) {
            // caso não tenha encontrado para versão solicitada, retorna a
            // ultima versão vigente
            return serviceType.elementoNS()[0].elemento();
        }
        return null;
    }

    *//**
     * Retorna o namespace do serviço.
     * @param bean : objeto do serviço.
     * @return : retorna o namespace do objeto do serviço.
     *//*
    public final String xmlNamespace(final Object bean) {
        final NFeServiceType serviceType =
                bean.getClass().getAnnotation(NFeServiceType.class);
        if (serviceType.nomeNamespaceXML() == null
                || serviceType.nomeNamespaceXML().trim().length() == 0) {
            return null;
        }
        return serviceType.nomeNamespaceXML();
    }*/
}
