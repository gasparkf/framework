package br.com.gaspar.utils;

import java.util.Collection;

/**
 * 
 * @author gaspar
 *
 */
public class ColecaoUtil {
	
	/**
	 * Testa se a coleção passada está nula ou vazia
	 * @param colecao
	 * @return TRUE se a coleção for vazia ou estiver nula e FALSE em caso contrário
	 * 
	 * @author gaspar
	 */
	public static Boolean ehNuloOuVazio(Collection<?> colecao){
		Boolean ret = Boolean.TRUE;
		if(colecao != null && !colecao.isEmpty()){
			ret = Boolean.FALSE;
		}
		return ret;
	}

}
