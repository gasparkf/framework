package br.com.gaspar.utils;

/**
 * Classe utilitaria para chave de acesso da NFe,
 * retorna os componentes de uma chave de acesso
 * valida (44 caracteres), verifica o digito 
 * utilizando o metodo da NumeroUtil
 * @author gmnascimento
 */
public class ChaveAcessoUtil {

    public static String getCodigoUF(String chaveAcesso) {
        if (isChaveValida(chaveAcesso)) {
            return chaveAcesso.substring(0, 2);
        } else {
            return null;
        }
    }

    public static String getAnoMesEmissao(String chaveAcesso) {
        if (isChaveValida(chaveAcesso)) {
            return chaveAcesso.substring(2, 6);
        } else {
            return null;
        }
    }

    public static String getAnoEmissao(String chaveAcesso) {
        if (isChaveValida(chaveAcesso)) {
            return chaveAcesso.substring(2, 4);
        } else {
            return null;
        }
    }
    
    public static String getMesEmissao(String chaveAcesso) {
        if (isChaveValida(chaveAcesso)) {
            return chaveAcesso.substring(4, 6);
        } else {
            return null;
        }
    }

    public static String getCNPJEmitente(String chaveAcesso) {
        if (isChaveValida(chaveAcesso)) {
            return chaveAcesso.substring(6, 20);
        } else {
            return null;
        }
    }

    public static String getModelo(String chaveAcesso) {
        if (isChaveValida(chaveAcesso)) {
            return chaveAcesso.substring(20, 22);
        } else {
            return null;
        }
    }

    public static String getSerie(String chaveAcesso) {
        if (isChaveValida(chaveAcesso)) {
            return chaveAcesso.substring(22, 25);
        } else {
            return null;
        }
    }

    public static String getNumeroNFe(String chaveAcesso) {
        if (isChaveValida(chaveAcesso)) {
            return chaveAcesso.substring(25, 34);
        } else {
            return null;
        }
    }

   /** 
    * Manual de Orientação ao Contribuinte, versao 5 Marco 2012
    * 1 – Normal – emissão normal;
    * 2 – Contingência FS – emissão em contingência com impressão do
    *     DANFE em Formulário de Segurança;
    * 3 – Contingência SCAN – emissão em contingência no Sistema de
    *     Contingência do Ambiente Nacional – SCAN;
    * 4 – Contingência DPEC - emissão em contingência com envio da Declaração
    *     Prévia de Emissão em Contingência – DPEC;
    * 5 – Contingência FS-DA - emissão em contingência com impressão do
    *     DANFE em Formulário de Segurança para Impressão de Documento Auxiliar
    *     de Documento Fiscal Eletrônico (FS-DA);
    * 6 – Contingência SVC-AN, emissão em contingência na SEFAZ Virtual do
    *     Ambiente Nacional;
    * 7 – Contingência SVC-RS, emissão em contingência na SEFAZ Virtual do RS.
    */
    public static String getFormaEmissao(String chaveAcesso) {
        if (isChaveValida(chaveAcesso)) {
            return chaveAcesso.substring(34, 35);
        } else {
            return null;
        }
    }

    public static String getCodigoNumerico(String chaveAcesso) {
        if (isChaveValida(chaveAcesso)) {
            return chaveAcesso.substring(35, 43);
        } else {
            return null;
        }
    }

    public static String getCodigoNumerico110(String chaveAcesso) {
        if (isChaveValida(chaveAcesso)) {
            return chaveAcesso.substring(34, 43);
        } else {
            return null;
        }
    }

    public static String getDigito(String chaveAcesso) {
        if (isChaveValida(chaveAcesso)) {
            return chaveAcesso.substring(43, 44);
        } else {
            return null;
        }
    }
    
    public static Boolean isDigitoValido(String chaveAcesso) {
        return NumeroUtil.verificaDigitoVerificadorChaveAcesso(chaveAcesso);
    }

    private static Boolean isChaveValida(String chaveAcesso) {
        if (null != chaveAcesso && chaveAcesso.length() == 44) {
            return true;
        } else {
            return false;
        }
    }
    

}
