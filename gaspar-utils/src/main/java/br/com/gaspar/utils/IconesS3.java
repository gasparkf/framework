package br.com.gaspar.utils;

/**
 * Classe utilitaria para devolver URLs no Amazon S3 de imagens
 * @author Gaspar
 *
 */
public class IconesS3 {
	
	private static final String[] iconesMapa = new String[]{
			"map_marker_azul_32.png", 
			"map_marker_verde_32.png", 
			"map_marker_rosa_32.png", 
			"map_marker_azul2_32.png", 
			"map_marker_verde2_32.png", 
			"map_marker_rosa2_32.png",
			"map_marker_azul3_32.png", 
			"map_marker_verde3_32.png", 
			"map_marker_rosa3_32.png",
			"map_marker_preto_32.png", 
			"map_marker_laranja_32.png"};
	
	private static final String S3 = "https://s3.amazonaws.com/carrera/";
	
	public static int getTotalIconesMapa(){
		return iconesMapa.length;
	}
	
	public static String getURLIconeMapa(Integer posicao){
		String url = null;
		if(posicao < getTotalIconesMapa()){
			url = S3 + iconesMapa[posicao];
		}
		return url;
	}

}
