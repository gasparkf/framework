package br.com.gaspar.utils;

import java.math.BigDecimal;

import javax.money.CurrencyUnit;
import javax.money.Monetary;
import javax.money.MonetaryAmount;

import org.javamoney.moneta.Money;
import org.javamoney.moneta.function.MonetaryUtil;

public class DinheiroUtil {
	
	private static final CurrencyUnit REAL = Monetary.getCurrency("BRL");
	
	public static BigDecimal somar(BigDecimal valor1, BigDecimal valor2){
		BigDecimal retorno = null;
		
		MonetaryAmount ma1 = Money.of(valor1, REAL);
		MonetaryAmount ma2 = Money.of(valor2, REAL);
		
		retorno = ma1.add(ma2).getNumber().numberValue(BigDecimal.class);
		
		return retorno;
		
	}

	public static BigDecimal subtrair(BigDecimal valor1, BigDecimal valor2) {
		BigDecimal retorno = null;
		
		MonetaryAmount ma1 = Money.of(valor1, REAL);
		MonetaryAmount ma2 = Money.of(valor2, REAL);
		
		retorno = ma1.subtract(ma2).getNumber().numberValue(BigDecimal.class);
		
		return retorno;
	}

	public static BigDecimal multiplicar(BigDecimal valor1, BigDecimal valor2) {
		BigDecimal retorno = null;
		
		MonetaryAmount ma1 = Money.of(valor1, REAL);
		MonetaryAmount ma2 = Money.of(valor2, REAL);
		
		retorno = ma1.multiply(ma2.getNumber()).getNumber().numberValue(BigDecimal.class);
		
		return retorno;
	}

	public static BigDecimal dividir(BigDecimal valor1, BigDecimal valor2) {
		BigDecimal retorno = null;
		
		MonetaryAmount ma1 = Money.of(valor1, REAL);
		MonetaryAmount ma2 = Money.of(valor2, REAL);
		
		retorno = ma1.divide(ma2.getNumber()).getNumber().numberValue(BigDecimal.class);
		
		return retorno;
	}

	public static BigDecimal getDigitos(BigDecimal valor1) {
		BigDecimal retorno = null;
		
		MonetaryAmount ma1 = Money.of(valor1, REAL);
		
		retorno = ma1.with(MonetaryUtil.minorPart()).getNumber().numberValue(BigDecimal.class);
		
		return retorno;
	}
	
	public static BigDecimal calcularPercentual(BigDecimal valor1, BigDecimal percentual) {
		BigDecimal retorno = null;
		
		MonetaryAmount ma1 = Money.of(valor1, REAL);
		
		MonetaryAmount desconto = ma1.with(MonetaryUtil.percent(percentual));
 		 
		retorno = desconto.getNumber().numberValue(BigDecimal.class);
		
		return retorno;
	}
}
