package br.com.gaspar.utils.entidade.iface;

import java.io.Serializable;

/**
 * Interface de Entidade Base para todas as entidades de um projeto
 * 
 * @author gaspar
 *
 */
public interface IEntidadeBase extends Serializable{
	
	void setId(Long id);
	
	Long getId();

}
