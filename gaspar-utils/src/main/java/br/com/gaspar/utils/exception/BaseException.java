package br.com.gaspar.utils.exception;

import java.util.Set;

import javax.validation.ConstraintViolation;

/**
 * 
 * @author gaspar
 *
 */
public class BaseException extends Exception {

    /**
     *
     */
    private static final long serialVersionUID = 8521241030187568540L;
    
    private Throwable erro;
    
    /**
     * código da rejeicao.
     */
    protected Integer codigo;
    /**
     * descrição da rejeição.
     */
    protected String descricao;
    
    protected Set<ConstraintViolation<?>> constraints;

    public BaseException() {
    }
    
    public BaseException(String descricao) {
    	this.descricao = descricao;
    }

    public BaseException(Throwable erro,int codigo, String descricao) {
        super();
        this.codigo = codigo;
        this.erro = erro;
        this.descricao = descricao;
    }

    public BaseException(int codigo, String descricao) {
        super();
        this.codigo = codigo;
        this.descricao = descricao;
    }

    public BaseException(Throwable erro) {
        super(erro);
        this.erro = erro;
    }
    
    public BaseException(Throwable erro, String descricao) {
        super(erro);
        this.erro = erro;
        this.descricao = descricao;
    }
    
    public BaseException(Set<ConstraintViolation<?>> constraints) {
    	this.constraints = constraints;
    }

    public Throwable getErro() {
        return erro;
    }

    public void setErro(Throwable erro) {
        this.erro = erro;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    /**
     * @return the codigo
     */
    public Integer getCodigo() {
        return codigo;
    }

    /**
     * @param codigo the codigo to set
     */
    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

	public Set<ConstraintViolation<?>> getConstraints() {
		return constraints;
	}

	public void setConstraints(Set<ConstraintViolation<?>> constraints) {
		this.constraints = constraints;
	}
}
