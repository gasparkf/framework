package br.com.gaspar.utils.entidade;


import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

/**
 * Classe abstrata base para projetos multi inquilinos que usam o mesmo banco para todos.
 * 
 * @author gaspar
 *
 */
@MappedSuperclass
public abstract class InquilinoEntidadeBase extends EntidadeBase {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Column(name="inquilino")
	private String inquilino;

	public String getInquilino() {
		return inquilino;
	}

	public void setInquilino(String inquilino) {
		this.inquilino = inquilino;
	}
}
