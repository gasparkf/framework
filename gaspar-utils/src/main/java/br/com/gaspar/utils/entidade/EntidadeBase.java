package br.com.gaspar.utils.entidade;


import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlTransient;

import br.com.gaspar.utils.entidade.iface.IEntidadeBase;

/**
 * Classe abstrata base de todas entidades persistentes
 * 
 * @author gaspar
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@MappedSuperclass
public abstract class EntidadeBase implements IEntidadeBase {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@XmlTransient
	@Version
	@Column(name="versao")
	protected Integer versao;

	public Integer getVersao() {
		return versao;
	}

	public void setVersao(Integer versao) {
		this.versao = versao;
	}
}
