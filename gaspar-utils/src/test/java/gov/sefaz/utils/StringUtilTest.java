/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gov.sefaz.utils;

import org.junit.Test;

import br.com.gaspar.utils.StringUtil;
import static org.junit.Assert.*;

/**
 *
 * @author vosouza
 */
public class StringUtilTest {

    public StringUtilTest() {
    }

    /**
     * Test of rtrim method, of class StringUtil.
     */
    @Test
    public void testRtrim() throws Exception {
        System.out.println("rtrim");
        String str = "vanessa  ";
        String expResult = "vanessa";
        String result = StringUtil.rtrim(str);
        assertEquals(expResult, result);
    }

    /**
     * Test of rtrim method, of class StringUtil.
     */
    @Test
    public void testRtrimComEspaco() throws Exception {
        System.out.println("rtrim");
        String str = "vanessa de  ";
        String expResult = "vanessa de";
        String result = StringUtil.rtrim(str);
        assertEquals(expResult, result);
    }

    /**
     * Test of rtrim method, of class StringUtil.
     */
    @Test
    public void testRtrimComEspacoEsquerda() throws Exception {
        System.out.println("rtrim");
        String str = "  vanessa";
        String expResult = "  vanessa";
        String result = StringUtil.rtrim(str);
        assertEquals(expResult, result);
    }
}
