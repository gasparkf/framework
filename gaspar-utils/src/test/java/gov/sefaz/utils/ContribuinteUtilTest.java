/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gov.sefaz.utils;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import br.com.gaspar.utils.ContribuinteUtil;

/**
 *
 * @author gmnascimento
 */
public class ContribuinteUtilTest {
    
    public ContribuinteUtilTest() {
    }

    /**
     * Test of getTipoContribuinte method, of class ContribuinteUtil.
     */
    @Test
    public void testGetTipoContribuinte() {
        System.out.println("getTipoContribuinte");
        String inscricaoEstadual = "280013752";
        String expResult = "CCI";
        String result = ContribuinteUtil.getTipoContribuinte(inscricaoEstadual);
        assertEquals(expResult, result);

        inscricaoEstadual = "285000578";
        expResult = "CAP";
        result = ContribuinteUtil.getTipoContribuinte(inscricaoEstadual);
        assertEquals(expResult, result);

        inscricaoEstadual = "185000578";
        expResult = null;
        result = ContribuinteUtil.getTipoContribuinte(inscricaoEstadual);
        assertEquals(expResult, result);
    }
}
