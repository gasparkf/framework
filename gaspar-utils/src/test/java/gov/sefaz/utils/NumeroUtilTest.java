/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gov.sefaz.utils;

import java.math.BigDecimal;
import org.junit.Test;

import br.com.gaspar.utils.NumeroUtil;
import static org.junit.Assert.*;

/**
 *
 * @author vosouza
 */
public class NumeroUtilTest {

    public NumeroUtilTest() {
    }

    /**
     * Test of FormatarDecimal method, of class NumeroUtil.
     */
    @Test
    public void testFormatar_double_int() {
        System.out.println("FormatarDecimal");
        double valor = 0.0;
        int decimais = 0;
        String expResult = "0";
        String result = NumeroUtil.formatar(valor, decimais);
        assertEquals(expResult, result);

        valor = 0.0;
        decimais = 1;
        expResult = "0,0";
        result = NumeroUtil.formatar(valor, decimais);
        assertEquals(expResult, result);

        valor = 256.8741;
        decimais = 2;
        expResult = "256,87";
        result = NumeroUtil.formatar(valor, decimais);
        assertEquals(expResult, result);

        valor = 256.8751;
        decimais = 2;
        expResult = "256,88";
        result = NumeroUtil.formatar(valor, decimais);
        assertEquals(expResult, result);
    }

    /**
     * Test of FormatarDecimal method, of class NumeroUtil.
     */
    @Test
    public void testFormatar_String_int() {
        System.out.println("FormatarDecimal");
        String valor = "0,0";
        int Decimais = 0;
        String expResult = "0";
        String result = NumeroUtil.formatar(valor, Decimais);
        assertEquals(expResult, result);

        valor = "0,0";
        Decimais = 1;
        expResult = "0,0";
        result = NumeroUtil.formatar(valor, Decimais);
        assertEquals(expResult, result);

        valor = "0.0";
        Decimais = 1;
        expResult = "0,0";
        result = NumeroUtil.formatar(valor, Decimais);
        assertEquals(expResult, result);

        valor = "256.8741";
        Decimais = 2;
        expResult = "256,87";
        result = NumeroUtil.formatar(valor, Decimais);
        assertEquals(expResult, result);

        valor = "1256.8751";
        Decimais = 2;
        expResult = "1.256,88";
        result = NumeroUtil.formatar(valor, Decimais);
        assertEquals(expResult, result);

        valor = null;
        Decimais = 2;
        result = NumeroUtil.formatar(valor, Decimais);
        assertNull(result);
    }

    /**
     * Test of acertarDouble method, of class NumeroUtil.
     */
    @Test
    public void testAcertarDouble() {
        System.out.println("acertarDouble");
        double Valor = 0.0;
        double expResult = 0.0;
        double result = NumeroUtil.acertarDouble(Valor);
        assertEquals(expResult, result, 0.0);

        Valor = 0.598;
        expResult = 0.60;
        result = NumeroUtil.acertarDouble(Valor);
        assertEquals(expResult, result, 0.0);

        Valor = 658.5489;
        expResult = 658.55;
        result = NumeroUtil.acertarDouble(Valor);
        assertEquals(expResult, result, 0.0);

        Valor = 1658.5449;
        expResult = 1658.54;
        result = NumeroUtil.acertarDouble(Valor);
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of formatarSemMilhar method, of class NumeroUtil.
     */
    @Test
    public void testFormatarSemMilhar() {
        System.out.println("formatarSemMilhar");
        double valor = 0.0;
        int decimais = 0;
        String expResult = "0";
        String result = NumeroUtil.formatarSemMilhar(valor, decimais);
        assertEquals(expResult, result);

        valor = 0.0;
        decimais = 1;
        expResult = "0,0";
        result = NumeroUtil.formatarSemMilhar(valor, decimais);
        assertEquals(expResult, result);

        valor = 256.8741;
        decimais = 2;
        expResult = "256,87";
        result = NumeroUtil.formatarSemMilhar(valor, decimais);
        assertEquals(expResult, result);

        valor = 1256.8751;
        decimais = 2;
        expResult = "1256,88";
        result = NumeroUtil.formatarSemMilhar(valor, decimais);
        assertEquals(expResult, result);
    }

    /**
     * Test of formatarQtde method, of class NumeroUtil.
     */
    @Test
    public void testFormatar_3args() {
        System.out.println("formatarQtde");
        double valor = 0.0;
        int decimaisMaximos = 0;
        int decimaisMinimos = 0;
        String expResult = "0";
        String result = NumeroUtil.formatar(valor, decimaisMaximos, decimaisMinimos);
        assertEquals(expResult, result);

        valor = 1000.99999959;
        decimaisMaximos = 7;
        decimaisMinimos = 2;
        expResult = "1.000,9999996";
        result = NumeroUtil.formatar(valor, decimaisMaximos, decimaisMinimos);
        assertEquals(expResult, result);

        valor = 1000.9;
        decimaisMaximos = 7;
        decimaisMinimos = 2;
        expResult = "1.000,90";
        result = NumeroUtil.formatar(valor, decimaisMaximos, decimaisMinimos);
        assertEquals(expResult, result);

        valor = 1000.9;
        decimaisMaximos = 1;
        decimaisMinimos = 3;
        expResult = "1.000,900";
        result = NumeroUtil.formatar(valor, decimaisMaximos, decimaisMinimos);
        assertEquals(expResult, result);
    }

     /**
     * Test of formatarQtdeAgruparMilhares method, of class NumeroUtil.
     */
    @Test
    public void testFormatarComMilhar() {
        System.out.println("formatarQtdeAgruparMilhares");
        double valor = 1000.59;
        int decimaisMaximos = 2;
        int decimaisMinimos = 2;
        String expResult = "1,00K";
        String result = NumeroUtil.formatarComMilhar(valor, decimaisMaximos, decimaisMinimos);
        assertEquals(expResult, result);

        valor = 10000.59;
        decimaisMaximos = 2;
        decimaisMinimos = 2;
        expResult = "10,00K";
        result = NumeroUtil.formatarComMilhar(valor, decimaisMaximos, decimaisMinimos);
        assertEquals(expResult, result);

        valor = 1000000.59;
        decimaisMaximos = 2;
        decimaisMinimos = 2;
        expResult = "1,00M";
        result = NumeroUtil.formatarComMilhar(valor, decimaisMaximos, decimaisMinimos);
        assertEquals(expResult, result);

        valor = 1000000000.59;
        decimaisMaximos = 2;
        decimaisMinimos = 2;
        expResult = "1,00Bi";
        result = NumeroUtil.formatarComMilhar(valor, decimaisMaximos, decimaisMinimos);
        assertEquals(expResult, result);
    }

    /**
     * Test of formatar method, of class NumeroUtil.
     */
    @Test
    public void testFormatar_double_String() {
        System.out.println("formatar");
        double numero = 0.0;
        String mascara = "0.00";
        String expResult = "0,00";
        String result = NumeroUtil.formatar(numero, mascara);
        assertEquals(expResult, result);

        numero = 10;
        mascara = "0.00";
        expResult = "10,00";
        result = NumeroUtil.formatar(numero, mascara);
        assertEquals(expResult, result);

        numero = 10.587;
        mascara = "0.00";
        expResult = "10,59";
        result = NumeroUtil.formatar(numero, mascara);
        assertEquals(expResult, result);

        numero = 10.587;
        mascara = "0.00#";
        expResult = "10,587";
        result = NumeroUtil.formatar(numero, mascara);
        assertEquals(expResult, result);

        numero = 10.587;
        mascara = "000.00#";
        expResult = "010,587";
        result = NumeroUtil.formatar(numero, mascara);
        assertEquals(expResult, result);

        numero = 10.587;
        mascara = null;
        expResult = "";
        result = NumeroUtil.formatar(numero, mascara);
        assertEquals(expResult, result);
    }

    /**
     * Test of formatar method, of class NumeroUtil.
     */
    @Test
    public void testFormatar_String_String() {
        System.out.println("formatar");
        String numero = "0.0";
        String mascara = "0.00";
        String expResult = "0,00";
        String result = NumeroUtil.formatar(numero, mascara);
        assertEquals(expResult, result);

        numero = "10";
        mascara = "0.00";
        expResult = "10,00";
        result = NumeroUtil.formatar(numero, mascara);
        assertEquals(expResult, result);

        numero = "10.587";
        mascara = "0.00";
        expResult = "10,59";
        result = NumeroUtil.formatar(numero, mascara);
        assertEquals(expResult, result);

        numero = "10.587";
        mascara = "0.00#";
        expResult = "10,587";
        result = NumeroUtil.formatar(numero, mascara);
        assertEquals(expResult, result);

        numero = "10.587";
        mascara = "000.00#";
        expResult = "010,587";
        result = NumeroUtil.formatar(numero, mascara);
        assertEquals(expResult, result);

        numero = "";
        mascara = "000.00#";
        expResult = "000,00";
        result = NumeroUtil.formatar(numero, mascara);
        assertEquals(expResult, result);

        numero = null;
        mascara = "000.00#";
        expResult = "";
        result = NumeroUtil.formatar(numero, mascara);
        assertEquals(expResult, result);
    }

    /**
     * Test of formatar method, of class NumeroUtil.
     */
    @Test
    public void testFormatar_int_String() {
        System.out.println("formatar");
        int numero = 0;
        String mascara = "0.00";
        String expResult = "0,00";
        String result = NumeroUtil.formatar(numero, mascara);
        assertEquals(expResult, result);

        numero = 10;
        mascara = "0.00";
        expResult = "10,00";
        result = NumeroUtil.formatar(numero, mascara);
        assertEquals(expResult, result);

        numero = 10;
        mascara = "0.00#";
        expResult = "10,00";
        result = NumeroUtil.formatar(numero, mascara);
        assertEquals(expResult, result);

        numero = 10;
        mascara = "000.00#";
        expResult = "010,00";
        result = NumeroUtil.formatar(numero, mascara);
        assertEquals(expResult, result);

        numero = 10;
        mascara = null;
        expResult = "";
        result = NumeroUtil.formatar(numero, mascara);
        assertEquals(expResult, result);
    }

    /**
     * Test of formatar method, of class NumeroUtil.
     */
    @Test
    public void testFormatar_long_String() {
        System.out.println("formatar");
        long numero = 0L;
        String mascara = "0.00";
        String expResult = "0,00";
        String result = NumeroUtil.formatar(numero, mascara);
        assertEquals(expResult, result);

        numero = 10L;
        mascara = "0.00";
        expResult = "10,00";
        result = NumeroUtil.formatar(numero, mascara);
        assertEquals(expResult, result);

        numero = 10L;
        mascara = "0.00#";
        expResult = "10,00";
        result = NumeroUtil.formatar(numero, mascara);
        assertEquals(expResult, result);

        numero = 10L;
        mascara = "000.00#";
        expResult = "010,00";
        result = NumeroUtil.formatar(numero, mascara);
        assertEquals(expResult, result);

        numero = 10L;
        mascara = null;
        expResult = "";
        result = NumeroUtil.formatar(numero, mascara);
        assertEquals(expResult, result);
    }


    /**
     * Test of FormatarCNPJ method, of class NumeroUtil.
     */
    @Test
    public void testFormatarCNPJ() {
        System.out.println("FormatarCNPJ");
        String Numero = "03442526000110";
        String expResult = "03.442.526/0001-10";
        String result = NumeroUtil.formatarCNPJ(Numero);
        assertEquals(expResult, result);

        Numero = "3442526000110";
        expResult = "03.442.526/0001-10";
        result = NumeroUtil.formatarCNPJ(Numero);
        assertEquals(expResult, result);

        Numero = "034425260001100";
        expResult = "03.442.526/0001-10";
        result = NumeroUtil.formatarCNPJ(Numero);
        assertEquals(expResult, result);

        Numero = "";
        expResult = "00.000.000/0000-00";
        result = NumeroUtil.formatarCNPJ(Numero);
        assertEquals(expResult, result);

        Numero = null;
        result = NumeroUtil.formatarCNPJ(Numero);
        assertNull(result);
    }


    /**
     * Test of FormatarCPF method, of class NumeroUtil.
     */
    @Test
    public void testFormatarCPF() {
        System.out.println("FormatarCPF");
        String Numero = "01008245100";
        String expResult = "010.082.451-00";
        String result = NumeroUtil.formatarCPF(Numero);
        assertEquals(expResult, result);

        Numero = "010082451000";
        expResult = "010.082.451-00";
        result = NumeroUtil.formatarCPF(Numero);
        assertEquals(expResult, result);

        Numero = "1008245100";
        expResult = "010.082.451-00";
        result = NumeroUtil.formatarCPF(Numero);
        assertEquals(expResult, result);

        Numero = "";
        expResult = "000.000.000-00";
        result = NumeroUtil.formatarCPF(Numero);
        assertEquals(expResult, result);

        Numero = null;
        result = NumeroUtil.formatarCPF(Numero);
        assertNull(result);
    }


    /**
     * Test of formatarFone method, of class NumeroUtil.
     */
    @Test
    public void testFormatarFone() {
        System.out.println("formatarFone");
        String fone = "6799810824";
        String expResult = "(67)9981-0824";
        String result = NumeroUtil.formatarFone(fone);
        assertEquals(expResult, result);

        fone = "67334172444";
        expResult = "(67)3341-72444";
        result = NumeroUtil.formatarFone(fone);
        assertEquals(expResult, result);

        fone = "";
        expResult = "";
        result = NumeroUtil.formatarFone(fone);
        assertEquals(expResult, result);

        fone = null;
        expResult = "";
        result = NumeroUtil.formatarFone(fone);
        assertEquals(expResult, result);
    }

    /**
     * Test of formatarCEP method, of class NumeroUtil.
     */
    @Test
    public void testFormatarCEP() {
        System.out.println("formatarCEP");
        String CEP = "79051835";
        String expResult = "79.051-835";
        String result = NumeroUtil.formatarCEP(CEP);
        assertEquals(expResult, result);

        CEP = "790518355";
        expResult = "79.051-8355";
        result = NumeroUtil.formatarCEP(CEP);
        assertEquals(expResult, result);

        CEP = "";
        expResult = "";
        result = NumeroUtil.formatarCEP(CEP);
        assertEquals(expResult, result);

        CEP = null;
        expResult = "";
        result = NumeroUtil.formatarCEP(CEP);
        assertEquals(expResult, result);
    }

    /**
     * Test of validaDigitoIESuframa method, of class NumeroUtil.
     */
    @Test
    public void testValidaDigitoIESuframa() {
        System.out.println("validaDigitoIESuframa");
        String suframa = "110261500";
        boolean expResult = true;
        boolean result = NumeroUtil.validaDigitoIESuframa(suframa);
        assertEquals(expResult, result);

        suframa = "110776500";
        expResult = true;
        result = NumeroUtil.validaDigitoIESuframa(suframa);
        assertEquals(expResult, result);

        suframa = "100599311";
        expResult = true;
        result = NumeroUtil.validaDigitoIESuframa(suframa);
        assertEquals(expResult, result);

        suframa = "101214502";
        expResult = true;
        result = NumeroUtil.validaDigitoIESuframa(suframa);
        assertEquals(expResult, result);

        suframa = "106066013";
        expResult = true;
        result = NumeroUtil.validaDigitoIESuframa(suframa);
        assertEquals(expResult, result);

        suframa = "100141323";
        expResult = true;
        result = NumeroUtil.validaDigitoIESuframa(suframa);
        assertEquals(expResult, result);

        suframa = "107278014";
        expResult = true;
        result = NumeroUtil.validaDigitoIESuframa(suframa);
        assertEquals(expResult, result);

        suframa = "100739105";
        expResult = true;
        result = NumeroUtil.validaDigitoIESuframa(suframa);
        assertEquals(expResult, result);

        suframa = "100653316";
        expResult = true;
        result = NumeroUtil.validaDigitoIESuframa(suframa);
        assertEquals(expResult, result);

        suframa = "112439306";
        expResult = true;
        result = NumeroUtil.validaDigitoIESuframa(suframa);
        assertEquals(expResult, result);

        suframa = "110903307";
        expResult = true;
        result = NumeroUtil.validaDigitoIESuframa(suframa);
        assertEquals(expResult, result);

        suframa = "100153208";
        expResult = true;
        result = NumeroUtil.validaDigitoIESuframa(suframa);
        assertEquals(expResult, result);

        suframa = "101201508";
        expResult = true;
        result = NumeroUtil.validaDigitoIESuframa(suframa);
        assertEquals(expResult, result);

        suframa = "101487509";
        expResult = true;
        result = NumeroUtil.validaDigitoIESuframa(suframa);
        assertEquals(expResult, result);

        suframa = "101487508";
        expResult = false;
        result = NumeroUtil.validaDigitoIESuframa(suframa);
        assertEquals(expResult, result);

        suframa = "1014875099";
        expResult = false;
        result = NumeroUtil.validaDigitoIESuframa(suframa);
        assertEquals(expResult, result);

        suframa = "10148750";
        expResult = false;
        result = NumeroUtil.validaDigitoIESuframa(suframa);
        assertEquals(expResult, result);

        suframa = "";
        expResult = false;
        result = NumeroUtil.validaDigitoIESuframa(suframa);
        assertEquals(expResult, result);


        suframa = null;
        expResult = false;
        result = NumeroUtil.validaDigitoIESuframa(suframa);
        assertEquals(expResult, result);

    }

    /**
     * Test of verificaDigitoVerificadorChaveAcesso method, of class NumeroUtil.
     */
    @Test
    public void testVerificaDigitoVerificadorChaveAcesso() {
        System.out.println("verificaDigitoVerificadorChaveAcesso");
        String vNum = "";
        boolean expResult = false;
        boolean result = NumeroUtil.verificaDigitoVerificadorChaveAcesso(vNum);
        assertEquals(expResult, result);

        vNum = null;
        expResult = false;
        result = NumeroUtil.verificaDigitoVerificadorChaveAcesso(vNum);
        assertEquals(expResult, result);

        vNum = "5011050099619900015055001000002085100002374";
        expResult = false;
        result = NumeroUtil.verificaDigitoVerificadorChaveAcesso(vNum);
        assertEquals(expResult, result);

        vNum = "501105009961990001505500100000208510000237466";
        expResult = false;
        result = NumeroUtil.verificaDigitoVerificadorChaveAcesso(vNum);
        assertEquals(expResult, result);

        vNum = "50110500996199000150550010000020851000023746";
        expResult = true;
        result = NumeroUtil.verificaDigitoVerificadorChaveAcesso(vNum);
        assertEquals(expResult, result);

        vNum = "50110503442526000110550100001364621009413049";
        expResult = true;
        result = NumeroUtil.verificaDigitoVerificadorChaveAcesso(vNum);
        assertEquals(expResult, result);
        vNum = "50110575904383011913550080000466961165254208";
        expResult = true;
        result = NumeroUtil.verificaDigitoVerificadorChaveAcesso(vNum);
        assertEquals(expResult, result);
        vNum = "50110500924845000173550010000100741292784777";
        expResult = true;
        result = NumeroUtil.verificaDigitoVerificadorChaveAcesso(vNum);
        assertEquals(expResult, result);
        vNum = "50110502517186000186550010000131441002235366";
        expResult = true;
        result = NumeroUtil.verificaDigitoVerificadorChaveAcesso(vNum);
        assertEquals(expResult, result);
        vNum = "50110507369685004770550010000281361101202315";
        expResult = true;
        result = NumeroUtil.verificaDigitoVerificadorChaveAcesso(vNum);
        assertEquals(expResult, result);
        vNum = "50110506965080000104550020000047911311755164";
        expResult = true;
        result = NumeroUtil.verificaDigitoVerificadorChaveAcesso(vNum);
        assertEquals(expResult, result);
        vNum = "50110503385268000187550020000176981592578033";
        expResult = true;
        result = NumeroUtil.verificaDigitoVerificadorChaveAcesso(vNum);
        assertEquals(expResult, result);
        vNum = "50110503385268000187550020000177101289636622";
        expResult = true;
        result = NumeroUtil.verificaDigitoVerificadorChaveAcesso(vNum);
        assertEquals(expResult, result);
        vNum = "50110515505704000193550020000216071391934771";
        expResult = true;
        result = NumeroUtil.verificaDigitoVerificadorChaveAcesso(vNum);
        assertEquals(expResult, result);
        vNum = "50110500549020000540550010000119391000119390";
        expResult = true;
        result = NumeroUtil.verificaDigitoVerificadorChaveAcesso(vNum);
        assertEquals(expResult, result);

        System.out.println("verificaDigitoVerificadorCTe");
        vNum = "52060433009911002506550120000007800267301615";
        expResult = true;
        result = NumeroUtil.verificaDigitoVerificadorChaveAcesso(vNum);
        assertEquals(expResult, result);
        vNum = "50110505821946000213570010000000530000572800";
        expResult = true;
        result = NumeroUtil.verificaDigitoVerificadorChaveAcesso(vNum);
        assertEquals(expResult, result);
        vNum = "50110503011765000115570010001069930001069931";
        expResult = true;
        result = NumeroUtil.verificaDigitoVerificadorChaveAcesso(vNum);
        assertEquals(expResult, result);
        vNum = "24110548740351001307570000000206422068139351";
        expResult = true;
        result = NumeroUtil.verificaDigitoVerificadorChaveAcesso(vNum);
        assertEquals(expResult, result);
        vNum = "50110503011765000115570010001069950001069952";
        expResult = true;
        result = NumeroUtil.verificaDigitoVerificadorChaveAcesso(vNum);
        assertEquals(expResult, result);
        vNum = "41110579810099001380570020000004077105251952";
        expResult = true;
        result = NumeroUtil.verificaDigitoVerificadorChaveAcesso(vNum);
        assertEquals(expResult, result);
        vNum = "50110503011765000115570010001069970001069973";
        expResult = true;
        result = NumeroUtil.verificaDigitoVerificadorChaveAcesso(vNum);
        assertEquals(expResult, result);
        vNum = "50110503011765000115570010001069900001069905";
        expResult = true;
        result = NumeroUtil.verificaDigitoVerificadorChaveAcesso(vNum);
        assertEquals(expResult, result);
        vNum = "35110546435293000807570010001263891361557325";
        expResult = true;
        result = NumeroUtil.verificaDigitoVerificadorChaveAcesso(vNum);
        assertEquals(expResult, result);
        vNum = "50110503011765000115570010001069920001069926";
        expResult = true;
        result = NumeroUtil.verificaDigitoVerificadorChaveAcesso(vNum);
        assertEquals(expResult, result);
        vNum = "50110503011765000115570010001069940001069947";
        expResult = true;
        result = NumeroUtil.verificaDigitoVerificadorChaveAcesso(vNum);
        assertEquals(expResult, result);
        vNum = "50110503011765000115570010001069960001069968";
        expResult = true;
        result = NumeroUtil.verificaDigitoVerificadorChaveAcesso(vNum);
        assertEquals(expResult, result);
        vNum = "50110503011765000115570010001069980001069989";
        expResult = true;
        result = NumeroUtil.verificaDigitoVerificadorChaveAcesso(vNum);
        assertEquals(expResult, result);

    }

    /**
     * Test of digitoVerificadorCodgPaisBACEN method, of class NumeroUtil.
     */
    @Test
    public void testDigitoVerificadorCodgPaisBACEN() {
        System.out.println("digitoVerificadorCodgPaisBACEN");
        String campo = "1504";
        boolean expResult = true;
        boolean result = NumeroUtil.digitoVerificadorCodgPaisBACEN(campo);
        assertEquals(expResult, result);

        campo = "1508";
        expResult = true;
        result = NumeroUtil.digitoVerificadorCodgPaisBACEN(campo);
        assertEquals(expResult, result);

        campo = "4525";
        expResult = true;
        result = NumeroUtil.digitoVerificadorCodgPaisBACEN(campo);
        assertEquals(expResult, result);

        campo = "3595";
        expResult = true;
        result = NumeroUtil.digitoVerificadorCodgPaisBACEN(campo);
        assertEquals(expResult, result);

        campo = "4985";
        expResult = true;
        result = NumeroUtil.digitoVerificadorCodgPaisBACEN(campo);
        assertEquals(expResult, result);

        campo = "6781";
        expResult = true;
        result = NumeroUtil.digitoVerificadorCodgPaisBACEN(campo);
        assertEquals(expResult, result);

        campo = "7370";
        expResult = true;
        result = NumeroUtil.digitoVerificadorCodgPaisBACEN(campo);
        assertEquals(expResult, result);

        campo = "";
        expResult = false;
        result = NumeroUtil.digitoVerificadorCodgPaisBACEN(campo);
        assertEquals(expResult, result);

        campo = null;
        expResult = false;
        result = NumeroUtil.digitoVerificadorCodgPaisBACEN(campo);
        assertEquals(expResult, result);

        campo = "123";
        expResult = false;
        result = NumeroUtil.digitoVerificadorCodgPaisBACEN(campo);
        assertEquals(expResult, result);

        campo = "1058";
        expResult = true;
        result = NumeroUtil.digitoVerificadorCodgPaisBACEN(campo);
        assertEquals(expResult, result);

        campo = "0639";
        expResult = true;
        result = NumeroUtil.digitoVerificadorCodgPaisBACEN(campo);
        assertEquals(expResult, result);

        campo = "639";
        expResult = true;
        result = NumeroUtil.digitoVerificadorCodgPaisBACEN(campo);
        assertEquals(expResult, result);

        campo = "5860";
        expResult = true;
        result = NumeroUtil.digitoVerificadorCodgPaisBACEN(campo);
        assertEquals(expResult, result);

        campo = "1050";
        expResult = false;
        result = NumeroUtil.digitoVerificadorCodgPaisBACEN(campo);
        assertEquals(expResult, result);

        campo = "0630";
        expResult = false;
        result = NumeroUtil.digitoVerificadorCodgPaisBACEN(campo);
        assertEquals(expResult, result);

    }

    /**
     * Test of digitoVerificadorCodgMunicipioIBGE method, of class NumeroUtil.
     */
    @Test
    public void testDigitoVerificadorCodgMunicipioIBGE() {
        System.out.println("digitoVerificadorCodgMunicipioIBGE");
        String campo = "4305871";
        boolean expResult = true;
        boolean result = NumeroUtil.digitoVerificadorCodgMunicipioIBGE(campo);
        assertEquals(expResult, result);

        campo = "2201919";
        expResult = true;
        result = NumeroUtil.digitoVerificadorCodgMunicipioIBGE(campo);
        assertEquals(expResult, result);

        campo = "2202251";
        expResult = true;
        result = NumeroUtil.digitoVerificadorCodgMunicipioIBGE(campo);
        assertEquals(expResult, result);

        campo = "2201988";
        expResult = true;
        result = NumeroUtil.digitoVerificadorCodgMunicipioIBGE(campo);
        assertEquals(expResult, result);

        campo = "2611533";
        expResult = true;
        result = NumeroUtil.digitoVerificadorCodgMunicipioIBGE(campo);
        assertEquals(expResult, result);

        campo = "3117836";
        expResult = true;
        result = NumeroUtil.digitoVerificadorCodgMunicipioIBGE(campo);
        assertEquals(expResult, result);

        campo = "3152131";
        expResult = true;
        result = NumeroUtil.digitoVerificadorCodgMunicipioIBGE(campo);
        assertEquals(expResult, result);

        campo = "5203939";
        expResult = true;
        result = NumeroUtil.digitoVerificadorCodgMunicipioIBGE(campo);
        assertEquals(expResult, result);

        campo = "5203962";
        expResult = true;
        result = NumeroUtil.digitoVerificadorCodgMunicipioIBGE(campo);
        assertEquals(expResult, result);

        campo = null;
        expResult = false;
        result = NumeroUtil.digitoVerificadorCodgMunicipioIBGE(campo);
        assertEquals(expResult, result);

        campo = "";
        expResult = false;
        result = NumeroUtil.digitoVerificadorCodgMunicipioIBGE(campo);
        assertEquals(expResult, result);

        campo = "123456";
        expResult = false;
        result = NumeroUtil.digitoVerificadorCodgMunicipioIBGE(campo);
        assertEquals(expResult, result);

        campo = "1234567";
        expResult = false;
        result = NumeroUtil.digitoVerificadorCodgMunicipioIBGE(campo);
        assertEquals(expResult, result);

        campo = "5002704";
        expResult = true;
        result = NumeroUtil.digitoVerificadorCodgMunicipioIBGE(campo);
        assertEquals(expResult, result);

        campo = "3550308";
        expResult = true;
        result = NumeroUtil.digitoVerificadorCodgMunicipioIBGE(campo);
        assertEquals(expResult, result);

        campo = "1302603";
        expResult = true;
        result = NumeroUtil.digitoVerificadorCodgMunicipioIBGE(campo);
        assertEquals(expResult, result);

        campo = "4314902";
        expResult = true;
        result = NumeroUtil.digitoVerificadorCodgMunicipioIBGE(campo);
        assertEquals(expResult, result);

        campo = "5002700";
        expResult = false;
        result = NumeroUtil.digitoVerificadorCodgMunicipioIBGE(campo);
        assertEquals(expResult, result);

        campo = "3550300";
        expResult = false;
        result = NumeroUtil.digitoVerificadorCodgMunicipioIBGE(campo);
        assertEquals(expResult, result);

        campo = "1302600";
        expResult = false;
        result = NumeroUtil.digitoVerificadorCodgMunicipioIBGE(campo);
        assertEquals(expResult, result);

        campo = "4314900";
        expResult = false;
        result = NumeroUtil.digitoVerificadorCodgMunicipioIBGE(campo);
        assertEquals(expResult, result);
    }


    /**
     * Test of toLong method, of class NumeroUtil.
     */
    @Test
    public void testToLong() {
        System.out.println("toLong");
        int numero = 0;
        Long expResult = 0L;
        Long result = NumeroUtil.toLong(numero);
        assertEquals(expResult, result);

        numero = 10;
        expResult = 10L;
        result = NumeroUtil.toLong(numero);
        assertEquals(expResult, result);
    }

    /**
     * Test of getPattern method, of class NumeroUtil.
     */
    @SuppressWarnings("unused")
	@Test
    public void testGetPattern() {
        System.out.println("getPattern");
        int maxInt = 0;
        int maxDec = 0;
        boolean digAgrupa = false;
        String expResult = "#######0.00";
        String result = NumeroUtil.getPattern(10, 2, false);
        assertEquals(expResult, result);
    }

    /**
     * Test of preencheZeroEsquerda method, of class NumeroUtil.
     */
    @Test
    public void testPreencheZeroEsquerda() {
        System.out.println("preencheZeroEsquerda");
        String numero = "5";
        int len = 1;
        String expResult = "5";
        String result = NumeroUtil.preencheZeroEsquerda(numero, len);
        assertEquals(expResult, result);

        numero = "5";
        len = 3;
        expResult = "005";
        result = NumeroUtil.preencheZeroEsquerda(numero, len);
        assertEquals(expResult, result);

        numero = null;
        len = 1;
        expResult = "0";
        result = NumeroUtil.preencheZeroEsquerda(numero, len);
        assertEquals(expResult, result);

        numero = "";
        len = 2;
        expResult = "00";
        result = NumeroUtil.preencheZeroEsquerda(numero, len);
        assertEquals(expResult, result);
    }

    /**
     * Test of getNumber method, of class NumeroUtil.
     */
    @Test
    public void testGetNumber_String_int() {
        System.out.println("getNumber");
        String numero = "6953,589";
        int maxDec = 2;
        BigDecimal expResult = new BigDecimal("6953.58");
        BigDecimal result = NumeroUtil.getNumber(numero, maxDec);
        assertEquals(expResult, result);

        numero = "";
        maxDec = 2;
        expResult = new BigDecimal("0.00");
        result = NumeroUtil.getNumber(numero, maxDec);
        assertEquals(expResult, result);

        numero = null;
        maxDec = 2;
        expResult = new BigDecimal("0.00");
        result = NumeroUtil.getNumber(numero, maxDec);
        assertEquals(expResult, result);
    }

    /**
     * Test of getNumberFromBigDecimalConverter method, of class NumeroUtil.
     */
    @Test
    public void testGetNumberFromBigDecimalConverter() {
        System.out.println("getNumberFromBigDecimalConverter");
        String valor = "6953.589";
        int maxDec = 2;
        String expResult = "6953,58";
        String result = NumeroUtil.getNumberFromBigDecimalConverter(valor, maxDec);
        assertEquals(expResult, result);

        valor = "";
        maxDec = 2;
        expResult = "0,00";
        result = NumeroUtil.getNumberFromBigDecimalConverter(valor, maxDec);
        assertEquals(expResult, result);

        valor = null;
        maxDec = 2;
        expResult = "0,00";
        result = NumeroUtil.getNumberFromBigDecimalConverter(valor, maxDec);
        assertEquals(expResult, result);
    }

    /**
     * Test of getNumber method, of class NumeroUtil.
     */
    @Test
    public void testGetNumber_4args_1() {
        System.out.println("getNumber");
        String numero = "6953,589";
        int maxInt = 10;
        int maxDec = 2;
        boolean digAgrupa = false;
        String expResult = "6953,58";
        String result = NumeroUtil.formatar(numero, maxInt, maxDec, digAgrupa);
        assertEquals(expResult, result);

        numero = "";
        maxInt = 10;
        maxDec = 2;
        digAgrupa = false;
        expResult = "0,00";
        result = NumeroUtil.formatar(numero, maxInt, maxDec, digAgrupa);
        assertEquals(expResult, result);

        numero = null;
        maxInt = 10;
        maxDec = 2;
        digAgrupa = false;
        expResult = "0,00";
        result = NumeroUtil.formatar(numero, maxInt, maxDec, digAgrupa);
        assertEquals(expResult, result);
    }

    /**
     * Test of getNumber method, of class NumeroUtil.
     */
    @Test
    public void testGetNumber_4args_2() {
        System.out.println("getNumber");
        BigDecimal bd = new BigDecimal("6953.589");
        int maxInt = 10;
        int maxDec = 2;
        boolean digAgrupa = false;
        String expResult = "6953,58";
        String result = NumeroUtil.formatar(bd, maxInt, maxDec, digAgrupa);
        assertEquals(expResult, result);

        bd = null;
        maxInt = 10;
        maxDec = 2;
        digAgrupa = false;
        expResult = "0,00";
        result = NumeroUtil.formatar(bd, maxInt, maxDec, digAgrupa);
        assertEquals(expResult, result);
    }
    
        /**
     * Test of digitoVerificadorCean method, of class NumeroUtil.
     */
    @Test
    public void testDigitoVerificadorCean() {
        System.out.println("digitoVerificadorCean");
        String cean = "7891000315507";
        boolean expResult = true;
        boolean result = NumeroUtil.digitoVerificadorCean(cean);
        assertEquals(expResult, result);

        cean = "15897206";
        expResult = true;
        result = NumeroUtil.digitoVerificadorCean(cean);
        assertEquals(expResult, result);

        cean = "088475322780";
        expResult = true;
        result = NumeroUtil.digitoVerificadorCean(cean);
        assertEquals(expResult, result);

        cean = "4572200457787";
        expResult = true;
        result = NumeroUtil.digitoVerificadorCean(cean);
        assertEquals(expResult, result);

        cean = "00447514798229";
        expResult = true;
        result = NumeroUtil.digitoVerificadorCean(cean);
        assertEquals(expResult, result);

        //valores invalidos
        cean = "";
        expResult = false;
        result = NumeroUtil.digitoVerificadorCean(cean);
        assertEquals(expResult, result);

        cean = null;
        expResult = false;
        result = NumeroUtil.digitoVerificadorCean(cean);
        assertEquals(expResult, result);

        cean = "00000000";
        expResult = false;
        result = NumeroUtil.digitoVerificadorCean(cean);
        assertEquals(expResult, result);

        
        //fora dos limites
        cean = "4572200";
        expResult = false;
        result = NumeroUtil.digitoVerificadorCean(cean);
        assertEquals(expResult, result);

        cean = "00447514798221";
        expResult = false;
        result = NumeroUtil.digitoVerificadorCean(cean);
        assertEquals(expResult, result);

        cean = "0123456789";
        expResult = false;
        result = NumeroUtil.digitoVerificadorCean(cean);
        assertEquals(expResult, result);
        
        //digito invalido
        cean = "7891000315506";
        expResult = false;
        result = NumeroUtil.digitoVerificadorCean(cean);
        assertEquals(expResult, result);
        
        cean = "15897205";
        expResult = false;
        result = NumeroUtil.digitoVerificadorCean(cean);
        assertEquals(expResult, result);

        cean = "088405322780";
        expResult = false;
        result = NumeroUtil.digitoVerificadorCean(cean);
        assertEquals(expResult, result);

        cean = "4572210457787";
        expResult = false;
        result = NumeroUtil.digitoVerificadorCean(cean);
        assertEquals(expResult, result);

        cean = "0044751479823";
        expResult = false;
        result = NumeroUtil.digitoVerificadorCean(cean);
        assertEquals(expResult, result);
    }

    @Test
    public void testGetNumeroRomano() {
    	Integer numero = 1;
    	String esperado = "I";
    	String resultado = NumeroUtil.getNumeroRomano(numero);
    	assertEquals(esperado, resultado);
    	
    	Integer numero2 = 10;
    	String esperado2 = "X";
    	String resultado2 = NumeroUtil.getNumeroRomano(numero2);
    	assertEquals(esperado2, resultado2);
    	
    	Integer numero3 = 16;
    	String esperado3 = null;
    	String resultado3 = NumeroUtil.getNumeroRomano(numero3);
    	assertEquals(esperado3, resultado3);
    }
    
    @Test
    public void testFormatarSemVirgula() {
    	Double numero = 500.00D;
    	Integer esperado = 50000;
    	Integer resultado = NumeroUtil.formatarSemVirgula(numero);
    	assertEquals(esperado, resultado);
    	
    	Double numero2 = 5500.00D;
    	Integer esperado2 = 550000;
    	Integer resultado2 = NumeroUtil.formatarSemVirgula(numero2);
    	assertEquals(esperado2, resultado2);
    	
    	Double numero3 = 10500.99D;
    	Integer esperado3 = 1050099;
    	Integer resultado3 = NumeroUtil.formatarSemVirgula(numero3);
    	assertEquals(esperado3, resultado3);
    }
}