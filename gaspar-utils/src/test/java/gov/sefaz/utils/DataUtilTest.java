/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gov.sefaz.utils;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.xml.datatype.XMLGregorianCalendar;

import org.junit.Test;

import br.com.gaspar.utils.DataUtil;

/**
 *
 * @author vosouza
 */
public class DataUtilTest {

    public DataUtilTest() {
    }

    /**
     * Test of getInstance method, of class DataUtil.
     */
    @Test
    public void testGetInstance() {
        System.out.println("getInstance");
        DataUtil result = DataUtil.getInstance();
        assertNotNull(result);
    }

    /**
     * Test of setAppender method, of class DataUtil.
     */
//    @Test
//    public void testSetAppender() {
//        System.out.println("setAppender");
//        DataUtil instance = DataUtil.getInstance();
//        instance.setAppender();
//    }

    /**
     * Test of getDiferencaEmDiasArrendodada method, of class DataUtil.
     */
    @SuppressWarnings("deprecation")
	@Test
    public void testGetDiferencaEmDiasArrendodada() {
        System.out.println("getDiferencaEmDiasArrendodada");
        Date dataInicial = DataUtil.getDate("01-05-2011", "dd-MM-yyyy");
        Date dataFinal = DataUtil.getDate("06-05-2011", "dd-MM-yyyy");
        Integer expResult = 5;
        Integer result = DataUtil.getDiferencaEmDiasArrendodada(dataInicial, dataFinal);
        assertEquals(expResult, result);

        dataInicial = DataUtil.getDate("01-05-2011 00:00", "dd-MM-yyyy HH:mm");
        dataFinal = DataUtil.getDate("01-05-2011 23:59", "dd-MM-yyyy HH:mm");
        expResult = 0;
        result = DataUtil.getDiferencaEmDiasArrendodada(dataInicial, dataFinal);
        assertEquals(expResult, result);

        dataInicial = DataUtil.getDate("01-05-2011 23:00", "dd-MM-yyyy HH:mm");
        dataFinal = DataUtil.getDate("30-04-2011 13:59", "dd-MM-yyyy HH:mm");
        expResult = -1;
        result = DataUtil.getDiferencaEmDiasArrendodada(dataInicial, dataFinal);
        assertEquals(expResult, result);

        dataInicial = null;
        dataFinal = null;
        expResult = null;
        result = DataUtil.getDiferencaEmDiasArrendodada(dataInicial, dataFinal);
        assertNull(result);
    }

    /**
     * Test of getDiferencaEmDias method, of class DataUtil.
     */
    @SuppressWarnings("deprecation")
	@Test
    public void testGetDiferencaEmDias() {
        System.out.println("getDiferencaEmDias");
        Date dataInicial = DataUtil.getDate("01-05-2011", "dd-MM-yyyy");
        Date dataFinal = DataUtil.getDate("06-05-2011", "dd-MM-yyyy");
        Float expResult = 5.0F;
        Float result = DataUtil.getDiferencaEmDias(dataInicial, dataFinal);
        assertEquals(expResult, result, 0.0);

        dataInicial = DataUtil.getDate("01-05-2011 00:00", "dd-MM-yyyy HH:mm");
        dataFinal = DataUtil.getDate("01-05-2011 23:59", "dd-MM-yyyy HH:mm");
        expResult = 0.999F;
        result = DataUtil.getDiferencaEmDias(dataInicial, dataFinal);
        assertEquals(expResult, result, 0.3);

        dataInicial = DataUtil.getDate("01-05-2011 23:00", "dd-MM-yyyy HH:mm");
        dataFinal = DataUtil.getDate("30-04-2011 13:59", "dd-MM-yyyy HH:mm");
        expResult = -1.375F;
        result = DataUtil.getDiferencaEmDias(dataInicial, dataFinal);
        assertEquals(expResult, result, 0.3);

        dataInicial = null;
        dataFinal = null;
        expResult = null;
        result = DataUtil.getDiferencaEmDias(dataInicial, dataFinal);
        assertNull(result);

    }

    /**
     * Test of getDiferencaEmSegundos method, of class DataUtil.
     */
    @Test
    public void testGetDiferencaEmSegundos() {
        System.out.println("getDiferencaEmSegundos");
        Date dataInicial = null;
        Date dataFinal = null;
        DataUtil instance = DataUtil.getInstance();
        Float result = instance.getDiferencaEmSegundos(dataInicial, dataFinal);
        assertNull(result);

        dataInicial = DataUtil.getDate("01-05-2011", "dd-MM-yyyy");
        dataFinal = DataUtil.getDate("06-05-2011", "dd-MM-yyyy");
        Float expResult = 432000.0F;
        result = instance.getDiferencaEmSegundos(dataInicial, dataFinal);
        assertEquals(expResult, result, 0.0);

        dataInicial = DataUtil.getDate("01-05-2011 22:59:00", "dd-MM-yyyy HH:mm:ss");
        dataFinal = DataUtil.getDate("01-05-2011 23:59:59", "dd-MM-yyyy HH:mm:ss");
        expResult = 3659.0F;
        result = instance.getDiferencaEmSegundos(dataInicial, dataFinal);
        assertEquals(expResult, result, 0.0);

        dataInicial = DataUtil.getDate("01-05-2011 22:59:59", "dd-MM-yyyy HH:mm:ss");
        dataFinal = DataUtil.getDate("01-05-2011 21:59:59", "dd-MM-yyyy HH:mm:ss");
        expResult = -3600.0F;
        result = instance.getDiferencaEmSegundos(dataInicial, dataFinal);
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of dataFimMaiorIni method, of class DataUtil.
     */
    @Test
    public void testDataFimMaiorIni() {
        System.out.println("dataFimMaiorIni");
        Date dataInicial = DataUtil.getDate("01-05-2011 21:59:59", "dd-MM-yyyy HH:mm:ss");
        Date dataFinal = DataUtil.getDate("01-05-2011 21:59:59", "dd-MM-yyyy HH:mm:ss");
        DataUtil instance = DataUtil.getInstance();
        boolean expResult = false;
        boolean result = instance.dataFimMaiorIni(dataInicial, dataFinal);
        assertEquals(expResult, result);

        dataInicial = DataUtil.getDate("06-05-2011 21:59:59", "dd-MM-yyyy HH:mm:ss");
        dataFinal = DataUtil.getDate("01-05-2011 21:59:59", "dd-MM-yyyy HH:mm:ss");
        expResult = false;
        result = instance.dataFimMaiorIni(dataInicial, dataFinal);
        assertEquals(expResult, result);

        dataInicial = DataUtil.getDate("01-05-2011 21:59:59", "dd-MM-yyyy HH:mm:ss");
        dataFinal = DataUtil.getDate("06-05-2011 21:59:59", "dd-MM-yyyy HH:mm:ss");
        expResult = true;
        result = instance.dataFimMaiorIni(dataInicial, dataFinal);
        assertEquals(expResult, result);

        dataInicial = null;
        dataFinal = null;
        expResult = false;
        result = instance.dataFimMaiorIni(dataInicial, dataFinal);
        assertEquals(expResult, result);
    }

    /**
     * Test of dataFimMaiorIgualIni method, of class DataUtil.
     */
    @Test
    public void testDataFimMaiorIgualIni() {
        System.out.println("dataFimMaiorIgualIni");
        Date dataInicial = DataUtil.getDate("01-05-2011 21:59:59", "dd-MM-yyyy HH:mm:ss");
        Date dataFinal = DataUtil.getDate("01-05-2011 21:59:59", "dd-MM-yyyy HH:mm:ss");
        DataUtil instance = DataUtil.getInstance();
        boolean expResult = true;
        boolean result = instance.dataFimMaiorIgualIni(dataInicial, dataFinal);
        assertEquals(expResult, result);

        dataInicial = DataUtil.getDate("06-05-2011 21:59:59", "dd-MM-yyyy HH:mm:ss");
        dataFinal = DataUtil.getDate("01-05-2011 21:59:59", "dd-MM-yyyy HH:mm:ss");
        expResult = false;
        result = instance.dataFimMaiorIgualIni(dataInicial, dataFinal);
        assertEquals(expResult, result);

        dataInicial = DataUtil.getDate("01-05-2011 21:59:59", "dd-MM-yyyy HH:mm:ss");
        dataFinal = DataUtil.getDate("06-05-2011 21:59:59", "dd-MM-yyyy HH:mm:ss");
        expResult = true;
        result = instance.dataFimMaiorIgualIni(dataInicial, dataFinal);
        assertEquals(expResult, result);

        dataInicial = null;
        dataFinal = null;
        expResult = false;
        result = instance.dataFimMaiorIgualIni(dataInicial, dataFinal);
        assertEquals(expResult, result);
    }

    /**
     * Test of dataFimMaiorIniEmSegundos method, of class DataUtil.
     */
    @Test
    public void testDataFimMaiorIniEmSegundos() {
        System.out.println("dataFimMaiorIniEmSegundos");
        Date dataInicial = DataUtil.getDate("01-05-2011 21:59:59", "dd-MM-yyyy HH:mm:ss");
        Date dataFinal = DataUtil.getDate("01-05-2011 21:59:59", "dd-MM-yyyy HH:mm:ss");
        DataUtil instance = DataUtil.getInstance();
        boolean expResult = false;
        boolean result = instance.dataFimMaiorIniEmSegundos(dataInicial, dataFinal);
        assertEquals(expResult, result);

        dataInicial = DataUtil.getDate("01-05-2011 21:59:00", "dd-MM-yyyy HH:mm:ss");
        dataFinal = DataUtil.getDate("01-05-2011 21:59:59", "dd-MM-yyyy HH:mm:ss");
        expResult = true;
        result = instance.dataFimMaiorIniEmSegundos(dataInicial, dataFinal);
        assertEquals(expResult, result);

        dataInicial = DataUtil.getDate("01-05-2011 21:59:59", "dd-MM-yyyy HH:mm:ss");
        dataFinal = DataUtil.getDate("01-05-2011 21:59:00", "dd-MM-yyyy HH:mm:ss");
        expResult = false;
        result = instance.dataFimMaiorIniEmSegundos(dataInicial, dataFinal);
        assertEquals(expResult, result);

        dataInicial = null;
        dataFinal = null;
        expResult = false;
        result = instance.dataFimMaiorIniEmSegundos(dataInicial, dataFinal);
        assertEquals(expResult, result);
    }

    /**
     * Test of dataFimMaiorOuIgualIniEmSegundos method, of class DataUtil.
     */
    @Test
    public void testDataFimMaiorOuIgualIniEmSegundos() {
        System.out.println("dataFimMaiorOuIgualIniEmSegundos");
        Date dataInicial = DataUtil.getDate("01-05-2011 21:59:59", "dd-MM-yyyy HH:mm:ss");
        Date dataFinal = DataUtil.getDate("01-05-2011 21:59:59", "dd-MM-yyyy HH:mm:ss");
        DataUtil instance = DataUtil.getInstance();
        boolean expResult = true;
        boolean result = instance.dataFimMaiorOuIgualIniEmSegundos(dataInicial, dataFinal);
        assertEquals(expResult, result);

        dataInicial = DataUtil.getDate("01-05-2011 21:59:00", "dd-MM-yyyy HH:mm:ss");
        dataFinal = DataUtil.getDate("01-05-2011 21:59:59", "dd-MM-yyyy HH:mm:ss");
        expResult = true;
        result = instance.dataFimMaiorOuIgualIniEmSegundos(dataInicial, dataFinal);
        assertEquals(expResult, result);

        dataInicial = DataUtil.getDate("01-05-2011 21:59:59", "dd-MM-yyyy HH:mm:ss");
        dataFinal = DataUtil.getDate("01-05-2011 21:59:00", "dd-MM-yyyy HH:mm:ss");
        expResult = false;
        result = instance.dataFimMaiorOuIgualIniEmSegundos(dataInicial, dataFinal);
        assertEquals(expResult, result);

        dataInicial = null;
        dataFinal = null;
        expResult = false;
        result = instance.dataFimMaiorOuIgualIniEmSegundos(dataInicial, dataFinal);
        assertEquals(expResult, result);
    }

    /**
     * Test of dataConformeAnos method, of class DataUtil.
     */
    @Test
    public void testDataConformeAnos() {
        System.out.println("dataConformeAnos");
        Date dataReferencia = DataUtil.getDate("09-05-2011", "dd-MM-yyyy");
        long numeroAnosMinimos = 5L;
        boolean noPassado = false;
        DataUtil instance = DataUtil.getInstance();
        Date expResult = DataUtil.getDate("09-05-2016", "dd-MM-yyyy");
        Date result = instance.dataConformeAnos(dataReferencia, numeroAnosMinimos, noPassado);
        assertTrue(expResult.getTime()/1000 == result.getTime()/1000);

        dataReferencia = DataUtil.getDate("09-05-2011", "dd-MM-yyyy");
        numeroAnosMinimos = 5L;
        noPassado = true;
        expResult = DataUtil.getDate("09-05-2006", "dd-MM-yyyy");
        result = instance.dataConformeAnos(dataReferencia, numeroAnosMinimos, noPassado);
        assertTrue(expResult.getTime()/1000 == result.getTime()/1000);

        dataReferencia = DataUtil.getDate("09-05-2011", "dd-MM-yyyy");
        numeroAnosMinimos = 0L;
        noPassado = true;
        expResult = DataUtil.getDate("09-05-2011", "dd-MM-yyyy");
        result = instance.dataConformeAnos(dataReferencia, numeroAnosMinimos, noPassado);
        assertTrue(expResult.getTime()/1000 == result.getTime()/1000);

        dataReferencia = DataUtil.getDate("09-05-2011", "dd-MM-yyyy");
        numeroAnosMinimos = 0L;
        noPassado = false;
        expResult = DataUtil.getDate("09-05-2011", "dd-MM-yyyy");
        result = instance.dataConformeAnos(dataReferencia, numeroAnosMinimos, noPassado);
        assertTrue(expResult.getTime()/1000 == result.getTime()/1000);

        dataReferencia = null;
        numeroAnosMinimos = 5L;
        noPassado = true;
        result = instance.dataConformeAnos(dataReferencia, numeroAnosMinimos, noPassado);
        assertNull(result);
    }

    /**
     * Test of getDataFimMesCorrente method, of class DataUtil.
     */
    @Test
    public void testGetDataFimMesCorrente() {
        System.out.println("getDataFimMesCorrente");
        Date dataAtual = DataUtil.getDate("09-05-2011", "dd-MM-yyyy");
        DataUtil instance = DataUtil.getInstance();
        Date expResult = DataUtil.getDate("31-05-2011 23:59:59", "dd-MM-yyyy HH:mm:ss");
        Date result = instance.getDataFimMesCorrente(dataAtual);
        assertTrue(expResult.getTime()/1000 == result.getTime()/1000);

        dataAtual = DataUtil.getDate("09-09-2011", "dd-MM-yyyy");
        expResult = DataUtil.getDate("30-09-2011 23:59:59", "dd-MM-yyyy HH:mm:ss");
        result = instance.getDataFimMesCorrente(dataAtual);
        assertTrue(expResult.getTime()/1000 == result.getTime()/1000);

        dataAtual = null;
        result = instance.getDataFimMesCorrente(dataAtual);
        assertNull(result);
    }

    /**
     * Test of getDate method, of class DataUtil.
     */
    @Test
    public void testGetDate_3args() {
        System.out.println("getDate");
        int ano = 2011;
        int mes = 5;
        int dia = 6;
        Date expResult = DataUtil.getDate("06/05/2011", "dd/MM/yyyy");
        Date result = DataUtil.getDate(ano, mes, dia);
        assertTrue(expResult.getTime()/1000 == result.getTime()/1000);

        ano = 2011;
        mes = 05;
        dia = 32;
        result = DataUtil.getDate(ano, mes, dia);
        assertNull(result);

    }

    /**
     * Test of getDate method, of class DataUtil.
     */
    @Test
    public void testGetDate_6args() {
        System.out.println("getDate");
        int ano = 2011;
        int mes = 5;
        int dia = 6;
        int hora = 15;
        int minuto = 31;
        int segundo = 25;
        
        Date expResult = DataUtil.getDate("06/05/2011 15:31:25", "dd/MM/yyyy HH:mm:ss");
        Date result = DataUtil.getDate(ano, mes, dia, hora, minuto, segundo);
        assertTrue(expResult.getTime()/1000 == result.getTime()/1000);

        ano = 2011;
        mes = 05;
        dia = 32;
        hora = 27;
        minuto = 31;
        segundo = 25;
        result = DataUtil.getDate(ano, mes, dia);
        assertNull(result);
    }

    /**
     * Test of getDate method, of class DataUtil.
     */
    @Test
    public void testGetDate_String_String() {
        System.out.println("getDate");
        String data = "06-05-2011 14:40:25";
        String patternTo = "dd-MM-yyyy HH:mm:ss";
        Date expResult = DataUtil.getDate(2011, 5, 6, 14, 40, 25);
        Date result = DataUtil.getDate(data, patternTo);
        assertTrue(expResult.getTime()/1000 == result.getTime()/1000);

        data = null;
        patternTo = "dd-MM-yyyy HH:mm:ss";
        result = DataUtil.getDate(data, patternTo);
        assertNull(result);

        data = "06-05-2011";
        patternTo = null;
        result = DataUtil.getDate(data, patternTo);
        assertNull(result);

        data = "";
        patternTo = "dd-MM-yyyy HH:mm:ss";
        result = DataUtil.getDate(data, patternTo);
        assertNull(result);

        data = "06-05-2011";
        patternTo = "";
        result = DataUtil.getDate(data, patternTo);
        assertNull(result);

        data = "06-13-2011 00:00:00";
        patternTo = "dd-MM-yyyy HH:mm:ss";
        expResult = null;
        result = DataUtil.getDate(data, patternTo);
        System.out.println(result);
        assertNull(result);

    }

    /**
     * Test of getDate method, of class DataUtil.
     */
    @Test
    public void testGetDate_String_int() throws Exception {
        System.out.println("getDate");
        String data = "06/05/2011";
        int dateFormat = DateFormat.SHORT;
        Date expResult = DataUtil.getDate(2011, 5, 6);
        Date result = DataUtil.getDate(data, dateFormat);
        assertTrue(expResult.getTime()/1000 == result.getTime()/1000);

        data = null;
        dateFormat = DateFormat.SHORT;
        result = DataUtil.getDate(data, dateFormat);
        assertNull(result);

        data = "";
        dateFormat = DateFormat.SHORT;
        result = DataUtil.getDate(data, dateFormat);
        assertNull(result);

        data = "06/05/2011";
        result = DataUtil.getDate(data, null);
        assertNull(result);

        data = "06/13/2011";
        dateFormat = DateFormat.SHORT;
        result = DataUtil.getDate(data, dateFormat);
        assertNull(result);
    }

    /**
     * Test of sysDate method, of class DataUtil.
     */
    @SuppressWarnings("unused")
	@Test
    public void testSysDate_0args() {
        System.out.println("sysDate");
        String expResult = "";
        String result = DataUtil.sysDate();
        assertNotNull(result);
    }

    /**
     * Test of sysDate method, of class DataUtil.
     */
    @SuppressWarnings("unused")
	@Test
    public void testSysDate_String() {
        System.out.println("sysDate");
        String mascara = "";
        String expResult = "";
        String result = DataUtil.sysDate(mascara);
        assertNotNull(result);
    }

    /**
     * Test of getStringFromDate method, of class DataUtil.
     */
    @Test
    public void testGetStringFromDate() {
        System.out.println("getStringFromDate");
        Date data = DataUtil.getDate("06-05-2011 13:21", "dd-MM-yyyy HH:mm");
        String patternTo = "yy/MM/dd";
        String expResult = "11/05/06";
        String result = DataUtil.getDate(data, patternTo);
        assertEquals(expResult, result);

        data = null;
        patternTo = "yy/MM/dd";
        expResult = "";
        result = DataUtil.getDate(data, patternTo);
        assertEquals(expResult, result);

        data =  DataUtil.getDate("06-05-2011 13:21", "dd-MM-yyyy HH:mm");
        patternTo = "";
        expResult = "";
        result = DataUtil.getDate(data, patternTo);
        assertEquals(expResult, result);

        data =  DataUtil.getDate("06-05-2011 13:21", "dd-MM-yyyy HH:mm");
        patternTo = null;
        expResult = "";
        result = DataUtil.getDate(data, patternTo);
        assertEquals(expResult, result);
    }

    /**
     * Test of transformDateToAnotherPattern method, of class DataUtil.
     */
    @Test
    public void testTransformDateToAnotherPattern() {
        System.out.println("transformDateToAnotherPattern");
        String data = "06-05-2011 11:22:00";
        String patternOf = "dd-MM-yyyy HH:mm:ss";
        String patternTo = "yyyy/MM/dd HH:mm";
        String expResult = "2011/05/06 11:22";
        String result = DataUtil.transformDateToAnotherPattern(data, patternOf, patternTo);
        assertEquals(expResult, result);

        data = "31-02-2011 11:22:00";
        patternOf = "dd-MM-yyyy HH:mm:ss";
        patternTo = "yyyy/MM/dd HH:mm";
        expResult = "";
        result = DataUtil.transformDateToAnotherPattern(data, patternOf, patternTo);
        assertEquals(expResult, result);

        data = "06-05-2011 11:22:00";
        patternOf = "";
        patternTo = "yyyy/MM/dd HH:mm";
        expResult = "";
        result = DataUtil.transformDateToAnotherPattern(data, patternOf, patternTo);
        assertEquals(expResult, result);

        data = "06-05-2011 11:22:00";
        patternOf = null;
        patternTo = null;
        expResult = "";
        result = DataUtil.transformDateToAnotherPattern(data, patternOf, patternTo);
        assertEquals(expResult, result);

        data = "06-05-2011 11:22:00";
        patternOf = "dd-MM-yyyy HH:mm:ss";
        patternTo = "";
        expResult = "";
        result = DataUtil.transformDateToAnotherPattern(data, patternOf, patternTo);
        assertEquals(expResult, result);
    }

    /**
     * Test of getNomeMes method, of class DataUtil.
     */
    /*@Test
    public void testGetNomeMes() {
        System.out.println("getNomeMes");
        Date data = DataUtil.getDate("06-05-2011", "dd-MM-yyyy");
        DataUtil instance = DataUtil.getInstance();
        String expResult = "Maio";
        String result = instance.getNomeMes(data);
        assertEquals(expResult, result);

        data = DataUtil.getDate("06-01-2011", "dd-MM-yyyy");
        expResult = "Janeiro";
        result = instance.getNomeMes(data);
        assertEquals(expResult, result);

        data = DataUtil.getDate("06-02-2011", "dd-MM-yyyy");
        expResult = "Fevereiro";
        result = instance.getNomeMes(data);
        assertEquals(expResult, result);

        data = DataUtil.getDate("06-03-2011", "dd-MM-yyyy");
        expResult = "Março";
        result = instance.getNomeMes(data);
        assertEquals(expResult, result);

        data = DataUtil.getDate("06-04-2011", "dd-MM-yyyy");
        expResult = "Abril";
        result = instance.getNomeMes(data);
        assertEquals(expResult, result);

        data = DataUtil.getDate("06-06-2011", "dd-MM-yyyy");
        expResult = "Junho";
        result = instance.getNomeMes(data);
        assertEquals(expResult, result);

        data = DataUtil.getDate("06-07-2011", "dd-MM-yyyy");
        expResult = "Julho";
        result = instance.getNomeMes(data);
        assertEquals(expResult, result);

        data = DataUtil.getDate("06-08-2011", "dd-MM-yyyy");
        expResult = "Agosto";
        result = instance.getNomeMes(data);
        assertEquals(expResult, result);

        data = DataUtil.getDate("06-09-2011", "dd-MM-yyyy");
        expResult = "Setembro";
        result = instance.getNomeMes(data);
        assertEquals(expResult, result);

        data = DataUtil.getDate("06-10-2011", "dd-MM-yyyy");
        expResult = "Outubro";
        result = instance.getNomeMes(data);
        assertEquals(expResult, result);

        data = DataUtil.getDate("06-11-2011", "dd-MM-yyyy");
        expResult = "Novembro";
        result = instance.getNomeMes(data);
        assertEquals(expResult, result);

        data = DataUtil.getDate("06-12-2011", "dd-MM-yyyy");
        expResult = "Dezembro";
        result = instance.getNomeMes(data);
        assertEquals(expResult, result);

        data = null;
        result = instance.getNomeMes(data);
        assertNull(result);

    }*/

    /**
     * Test of getListaDeDatas method, of class DataUtil.
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
	@Test
    public void testGetListaDeDatas() {
        System.out.println("getListaDeDatas");
        Date inicio = null;
        Date fim = null;
        DataUtil instance = DataUtil.getInstance();
        List expResult = new ArrayList();
        List result = instance.getListaDeDatas(inicio, fim);
        assertArrayEquals(expResult.toArray(), result.toArray());

        inicio = DataUtil.getDate("09-05-2011", "dd-MM-yyyy");
        fim = DataUtil.getDate("09-05-2011", "dd-MM-yyyy");
        expResult = new ArrayList();
        expResult.add(fim);
        result = instance.getListaDeDatas(inicio, fim);
        assertArrayEquals(expResult.toArray(), result.toArray());

        inicio = DataUtil.getDate("09-05-2011", "dd-MM-yyyy");
        fim = DataUtil.getDate("11-05-2011", "dd-MM-yyyy");
        expResult = new ArrayList();
        expResult.add(inicio);
        expResult.add(DataUtil.getDate("10-05-2011", "dd-MM-yyyy"));
        expResult.add(fim);
        result = instance.getListaDeDatas(inicio, fim);
        assertArrayEquals(expResult.toArray(), result.toArray());
    }

    /**
     * Test of elapsedTime method, of class DataUtil.
     */
    @SuppressWarnings("deprecation")
	@Test
    public void testElapsedTime() throws Exception {
        System.out.println("elapsedTime");
        Date inicio = DataUtil.getDate("09-05-2011 00:00:00", "dd-MM-yyy HH:mm:ss");
        Date fim = DataUtil.getDate("09-05-2011 00:00:05", "dd-MM-yyy HH:mm:ss");
        DataUtil instance = DataUtil.getInstance();
        String expResult = "00:00:05";
        String result = instance.elapsedTime(inicio, fim);
        assertEquals(expResult, result);

        inicio = DataUtil.getDate("09-05-2011 00:00:00", "dd-MM-yyy HH:mm:ss");
        fim = DataUtil.getDate("10-05-2011 00:00:05", "dd-MM-yyy HH:mm:ss");
        expResult = "24:00:05";
        result = instance.elapsedTime(inicio, fim);
        assertEquals(expResult, result);

        inicio = DataUtil.getDate("10-05-2011 00:00:00", "dd-MM-yyy HH:mm:ss");
        fim = DataUtil.getDate("09-05-2011 00:00:05", "dd-MM-yyy HH:mm:ss");
        expResult = "-23:-59:-55";
        result = instance.elapsedTime(inicio, fim);
        assertEquals(expResult, result);

        inicio = null;
        fim = null;
        result = instance.elapsedTime(inicio, fim);
        assertNull(result);
    }

    /**
     * 
     * @throws Exception
     */
    @Test
    public void testGetAno() throws Exception {
    	System.out.println("getAno");
    	int ano = DataUtil.getAno(new Date());
    	Calendar c = Calendar.getInstance();
    	int anoAtual = c.get(Calendar.YEAR);
    	assertEquals(ano, anoAtual);
    }
    
    /**
     * 
     * @throws Exception
     */
    @Test
    public void testGetDiferencaHoras() throws Exception {
    	System.out.println("getDiferencaHoras");
    	DateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
    	Date dataInicial = sdf.parse("01/08/2011 09:00:00");
    	Date dataFinal = sdf.parse("01/08/2011 17:00:00");
    	BigDecimal diferenca = DataUtil.getDiferencaHoras(dataInicial, dataFinal);
    	assertEquals(diferenca.intValue(), 8);
    }
    
    @Test
    public void testSomarDatas()throws Exception{
    	System.out.println("somarDatas");
    	DateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
    	Date dataInicial = sdf.parse("09/12/2013 09:00:00");
    	Date resultadoEsperado = sdf.parse("10/12/2013 09:00:00");
    	
    	Date resultado = null; 
    	resultado = DataUtil.somarDatas(dataInicial, Calendar.DAY_OF_MONTH, 1);
    	assertEquals(resultadoEsperado, resultado);
    	
    	resultado = DataUtil.somarDatas(dataInicial, Calendar.HOUR_OF_DAY, 24);
    	assertEquals(resultadoEsperado, resultado);
    	
    	Date resultadoEsperado2 = sdf.parse("09/11/2013 09:00:00");
    	resultado = DataUtil.somarDatas(dataInicial, Calendar.DAY_OF_MONTH, -30);
    	assertEquals(resultadoEsperado2, resultado);
    	
    }
    
    @Test
    public void testgetDiasEntreDatas()throws Exception{
    	System.out.println("getDiasEntreDatas");
    	DateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
    	Date dataInicial = sdf.parse("20/01/2014 09:00:00");
    	Date dataFinal = sdf.parse("21/01/2014 09:00:00");
    	Integer resultadoEsperado = 1;
    	
    	Integer resultado = null; 
    	resultado = DataUtil.getDiasEntreDatasJoda(dataInicial, dataFinal);
    	assertEquals(resultadoEsperado, resultado);
    	
    	//Resultado negativo quando dataInicial < dataFinal
    	dataInicial = sdf.parse("21/01/2014 09:00:00");
    	dataFinal = sdf.parse("20/01/2014 09:00:00");
    	resultadoEsperado = -1;
    	
    	resultado = DataUtil.getDiasEntreDatasJoda(dataInicial, dataFinal);
    	assertEquals(resultadoEsperado, resultado);
    	    	
    }
    
    @Test
    public void testgetAnosEntreDatas()throws Exception{
    	System.out.println("getAnosEntreDatas");
    	DateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
    	Date dataInicial = sdf.parse("19/12/2013");
    	Date dataFinal = sdf.parse("11/11/2014");
    	Integer resultadoEsperado = 0;
    	
    	Integer resultado = null; 
    	resultado = DataUtil.getAnosEntreDatasJoda(dataInicial, dataFinal);
    	assertEquals(resultadoEsperado, resultado);
    	
    	//Resultado negativo quando dataInicial < dataFinal
    	dataInicial = sdf.parse("11/11/2013");
    	dataFinal = sdf.parse("11/11/2014");
    	resultadoEsperado = 1;
    	
    	resultado = DataUtil.getAnosEntreDatasJoda(dataInicial, dataFinal);
    	assertEquals(resultadoEsperado, resultado);
    	
    	dataInicial = sdf.parse("24/01/1987");
    	dataFinal = sdf.parse("11/11/2014");
    	resultadoEsperado = 27;
    	
    	resultado = DataUtil.getAnosEntreDatasJoda(dataInicial, dataFinal);
    	assertEquals(resultadoEsperado, resultado);
    	    	
    }
    
    @Test
    public void testtoXMLGregorianCalendar()throws Exception{
    	
    	//Data 20/04/2016 14:39:39
    	Date data = DataUtil.getDate(2016, 4, 20, 14, 39, 39);
    	XMLGregorianCalendar resultado = DataUtil.toXMLGregorianCalendar(data);
    	int tz = resultado.getTimezone();
    	String str = resultado.toString();
    
    }
}