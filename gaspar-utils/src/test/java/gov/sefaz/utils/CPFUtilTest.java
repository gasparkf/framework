/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gov.sefaz.utils;

import org.junit.Test;

import br.com.gaspar.utils.CPFUtil;
import static org.junit.Assert.*;

/**
 *
 * @author vosouza
 */
public class CPFUtilTest {

    public CPFUtilTest() {
    }


    /**
     * Test of isValid method, of class CPFUtil.
     */
    @Test
    public void testIsValid() {
        System.out.println("isValid");
        String cpf = "01008245100";
        boolean expResult = true;
        boolean result = CPFUtil.isValid(cpf);
        assertEquals(expResult, result);

        cpf = "51194023134";
        expResult = true;
        result = CPFUtil.isValid(cpf);
        assertEquals(expResult, result);

        cpf = "0100824511";
        expResult = false;
        result = CPFUtil.isValid(cpf);
        assertEquals(expResult, result);

        cpf = "010082451000";
        expResult = false;
        result = CPFUtil.isValid(cpf);
        assertEquals(expResult, result);

        cpf = "01008245101";
        expResult = false;
        result = CPFUtil.isValid(cpf);
        assertEquals(expResult, result);

        cpf = "11111111111";
        expResult = false;
        result = CPFUtil.isValid(cpf);
        assertEquals(expResult, result);

        cpf = "22222222222";
        expResult = false;
        result = CPFUtil.isValid(cpf);
        assertEquals(expResult, result);

        cpf = "33333333333";
        expResult = false;
        result = CPFUtil.isValid(cpf);
        assertEquals(expResult, result);

        cpf = "44444444444";
        expResult = false;
        result = CPFUtil.isValid(cpf);
        assertEquals(expResult, result);

        cpf = "55555555555";
        expResult = false;
        result = CPFUtil.isValid(cpf);
        assertEquals(expResult, result);

        cpf = "66666666666";
        expResult = false;
        result = CPFUtil.isValid(cpf);
        assertEquals(expResult, result);

        cpf = "77777777777";
        expResult = false;
        result = CPFUtil.isValid(cpf);
        assertEquals(expResult, result);

        cpf = "88888888888";
        expResult = false;
        result = CPFUtil.isValid(cpf);
        assertEquals(expResult, result);

        cpf = "99999999999";
        expResult = false;
        result = CPFUtil.isValid(cpf);
        assertEquals(expResult, result);

        cpf = "00000000000";
        expResult = false;
        result = CPFUtil.isValid(cpf);
        assertEquals(expResult, result);

        cpf = "";
        expResult = false;
        result = CPFUtil.isValid(cpf);
        assertEquals(expResult, result);

        cpf = null;
        expResult = false;
        result = CPFUtil.isValid(cpf);
        assertEquals(expResult, result);

    }

}