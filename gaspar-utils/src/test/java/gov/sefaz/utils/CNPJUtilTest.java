/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gov.sefaz.utils;

import org.junit.Test;

import br.com.gaspar.utils.CNPJUtil;
import static org.junit.Assert.*;

/**
 *
 * @author vosouza
 */
public class CNPJUtilTest {

    public CNPJUtilTest() {
    }

    /**
     * Test of isValid method, of class CNPJUtil.
     */
    @Test
    public void testIsValid() {
        System.out.println("isValid");
        String cnpj = "02935843000105";
        boolean expResult = true;
        boolean result = CNPJUtil.isValid(cnpj);
        assertEquals(expResult, result);

        cnpj = "02935843000104";
        expResult = false;
        result = CNPJUtil.isValid(cnpj);
        assertEquals(expResult, result);

        cnpj = "0293584300010";
        expResult = false;
        result = CNPJUtil.isValid(cnpj);
        assertEquals(expResult, result);

        cnpj = "029358430001050";
        expResult = false;
        result = CNPJUtil.isValid(cnpj);
        assertEquals(expResult, result);

        cnpj = "0000000000000";
        expResult = false;
        result = CNPJUtil.isValid(cnpj);
        assertEquals(expResult, result);

        cnpj = "";
        expResult = false;
        result = CNPJUtil.isValid(cnpj);
        assertEquals(expResult, result);

        cnpj = null;
        expResult = false;
        result = CNPJUtil.isValid(cnpj);
        assertEquals(expResult, result);

    }

}