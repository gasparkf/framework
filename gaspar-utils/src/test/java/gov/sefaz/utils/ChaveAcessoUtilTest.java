/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gov.sefaz.utils;

import org.junit.*;

import br.com.gaspar.utils.ChaveAcessoUtil;
import static org.junit.Assert.*;

/**
 *
 * @author gmnascimento
 */
public class ChaveAcessoUtilTest {

    /*
     * CHAVAE..........: 50120300230492000373550060000000331000000339
     * UF..............: 50 
     * ANO/MES.........: 1203 
     * CNPJ............: 00230492000373 
     * MODELO..........: 55 
     * SERIE...........: 006
     * NUMERO..........: 000000033 
     * FORMA DE EMISSAO: 1 
     * CODIGO..........: 00000033 
     * SERIE...........: 9
     */
    String chaveAcesso = "50120300230492000373550060000000331000000339";

    public ChaveAcessoUtilTest() {
    }

    /**
     * Test of getCodigoUF method, of class ChaveAcessoUtil.
     */
    @Test
    public void testGetCodigoUF() {
        System.out.println("getCodigoUF");
        String chaveNula = null;
        String chaveImcompleta = "5012030023049200037355006000000331000000339";
        String expResult = "50";
        String result = ChaveAcessoUtil.getCodigoUF(chaveAcesso);
        assertEquals(expResult, result);

        expResult = null;
        result = ChaveAcessoUtil.getCodigoUF(chaveNula);
        assertEquals(expResult, result);

        expResult = null;
        result = ChaveAcessoUtil.getCodigoUF(chaveImcompleta);
        assertEquals(expResult, result);
    }

    /**
     * Test of getAnoMesEmissao method, of class ChaveAcessoUtil.
     */
    @Test
    public void testGetAnoMesEmissao() {
        System.out.println("getAnoMesEmissao");
        String expResult = "1203";
        String result = ChaveAcessoUtil.getAnoMesEmissao(chaveAcesso);
        assertEquals(expResult, result);
    }

    /**
     * Test of getCNPJEmitente method, of class ChaveAcessoUtil.
     */
    @Test
    public void testGetCNPJEmitente() {
        System.out.println("getCNPJEmitente");
        String expResult = "00230492000373";
        String result = ChaveAcessoUtil.getCNPJEmitente(chaveAcesso);
        assertEquals(expResult, result);
    }

    /**
     * Test of getModelo method, of class ChaveAcessoUtil.
     */
    @Test
    public void testGetModelo() {
        System.out.println("getModelo");
        String expResult = "55";
        String result = ChaveAcessoUtil.getModelo(chaveAcesso);
        assertEquals(expResult, result);
    }

    /**
     * Test of getSerie method, of class ChaveAcessoUtil.
     */
    @Test
    public void testGetSerie() {
        System.out.println("getSerie");
        String expResult = "006";
        String result = ChaveAcessoUtil.getSerie(chaveAcesso);
        assertEquals(expResult, result);
    }

    /**
     * Test of getNumeroNFe method, of class ChaveAcessoUtil.
     */
    @Test
    public void testGetNumeroNFe() {
        System.out.println("getNumeroNFe");
        String expResult = "000000033";
        String result = ChaveAcessoUtil.getNumeroNFe(chaveAcesso);
        assertEquals(expResult, result);
    }

    /**
     * Test of getFormaEmissao method, of class ChaveAcessoUtil.
     */
    @Test
    public void testGetFormaEmissao() {
        System.out.println("getFormaEmissao");
        String expResult = "1";
        String result = ChaveAcessoUtil.getFormaEmissao(chaveAcesso);
        assertEquals(expResult, result);
    }

    /**
     * Test of getCodigoNumerico method, of class ChaveAcessoUtil.
     */
    @Test
    public void testGetCodigoNumerico() {
        System.out.println("getCodigoNumerico");
        String expResult = "00000033";
        String result = ChaveAcessoUtil.getCodigoNumerico(chaveAcesso);
        assertEquals(expResult, result);
    }

    /**
     * Test of getDigito method, of class ChaveAcessoUtil.
     */
    @Test
    public void testGetDigito() {
        System.out.println("getDigito");
        String expResult = "9";
        String result = ChaveAcessoUtil.getDigito(chaveAcesso);
        assertEquals(expResult, result);
    }

    /**
     * Test of isDigitoValido method, of class ChaveAcessoUtil.
     */
    @Test
    public void testIsDigitoValido() {
        System.out.println("isDigitoValido");
        Boolean expResult = Boolean.TRUE;
        Boolean result = ChaveAcessoUtil.isDigitoValido(chaveAcesso);
        assertEquals(expResult, result);
    }
}
