/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gov.sefaz.utils;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import br.com.gaspar.utils.VigenereUtil;

/**
 *
 * @author gaspar
 */
public class VigenereUtilTest {

    public VigenereUtilTest() {
    }

    /**
     * Test of rtrim method, of class StringUtil.
     */
    @Test
    public void testCipherEnc() throws Exception {
        System.out.println("testCipherEnc");
        String str = "PORSCHE123";
        String chave = "Gasparzinho";
        String expResult = "VOQRBGDCFI";
        String result = VigenereUtil.cipher(str, chave, true);
        assertEquals(expResult, result);
    }

    /**
     * Test of rtrim method, of class StringUtil.
     */
    @Test
    public void testCipherDec() throws Exception {
        System.out.println("testCipherDec");
        String str = "VOQRBGDCFI";
        String chave = "Gasparzinho";
        String expResult = "PORSCHE123";
        String result = VigenereUtil.cipher(str, chave, false);
        assertEquals(expResult, result);
    }
}
