package gov.sefaz.utils;

import java.math.BigDecimal;

import org.junit.Test;

import br.com.gaspar.utils.DinheiroUtil;
import junit.framework.Assert;

/**
 * 
 * @author gaspar
 *
 */
public class DinheiroUtilTest {
	
	@Test
	public void testSomar(){
		BigDecimal esperado = new BigDecimal("10");
		
		BigDecimal val1 = new BigDecimal("5");
		BigDecimal val2 = new BigDecimal("5");
		
		BigDecimal resultado = DinheiroUtil.somar(val1, val2);
		
		Assert.assertEquals(esperado, resultado);
		
		
		esperado = new BigDecimal("10.98");
		
		val1 = new BigDecimal("5.49");
		val2 = new BigDecimal("5.49");
		
		resultado = DinheiroUtil.somar(val1, val2);
		
		Assert.assertEquals(esperado, resultado);
	}
	
	@Test
	public void testSubtrair(){
		BigDecimal esperado = new BigDecimal("10");
		
		BigDecimal val1 = new BigDecimal("20");
		BigDecimal val2 = new BigDecimal("10");
		
		BigDecimal resultado = DinheiroUtil.subtrair(val1, val2);
		
		Assert.assertEquals(esperado, resultado);
		
		
		esperado = new BigDecimal("10.98");
		
		val1 = new BigDecimal("20");
		val2 = new BigDecimal("9.02");
		
		resultado = DinheiroUtil.subtrair(val1, val2);
		
		Assert.assertEquals(esperado, resultado);
	}
	
	@Test
	public void testMultiplicar(){
		BigDecimal esperado = new BigDecimal("10");
		
		BigDecimal val1 = new BigDecimal("5");
		BigDecimal val2 = new BigDecimal("2");
		
		BigDecimal resultado = DinheiroUtil.multiplicar(val1, val2);
		
		Assert.assertEquals(esperado, resultado);
		
		
		esperado = new BigDecimal("9.99");
		
		val1 = new BigDecimal("3.33");
		val2 = new BigDecimal("3");
		
		resultado = DinheiroUtil.multiplicar(val1, val2);
		
		Assert.assertEquals(esperado, resultado);
	}
	
	@Test
	public void testDividir(){
		BigDecimal esperado = new BigDecimal("5");
		
		BigDecimal val1 = new BigDecimal("10");
		BigDecimal val2 = new BigDecimal("2");
		
		BigDecimal resultado = DinheiroUtil.dividir(val1, val2);
		
		Assert.assertEquals(esperado, resultado);
		
		
		esperado = new BigDecimal("3.33");
		
		val1 = new BigDecimal("9.99");
		val2 = new BigDecimal("3");
		
		resultado = DinheiroUtil.dividir(val1, val2);
		
		Assert.assertEquals(esperado, resultado);
	}
	
	@Test
	public void testGetDigitos(){
		BigDecimal esperado = new BigDecimal("0.25");
		
		BigDecimal val1 = new BigDecimal("10.25");
		
		BigDecimal resultado = DinheiroUtil.getDigitos(val1);
		
		Assert.assertEquals(esperado, resultado);
	}
	
	/*@Test
	public void testCalcularPercentual(){
		BigDecimal esperado = new BigDecimal("60");
		
		BigDecimal valor1 = new BigDecimal("400");
		BigDecimal percentual = new BigDecimal("50");
		
		BigDecimal resultado = DinheiroUtil.calcularPercentual(valor1, percentual);
		
		Assert.assertEquals(esperado, resultado);
	}*/

}
