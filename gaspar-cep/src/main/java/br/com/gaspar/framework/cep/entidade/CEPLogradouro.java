package br.com.gaspar.framework.cep.entidade;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="cep_logradouro")
public class CEPLogradouro implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="logradouroid")
	private Integer id;

	@Column(name="siglauf")
	private String uf;

	@Column(name="localidadeid")
	private Integer localidadeId;

	@Column(name="bairroiniid")
	private Integer bairroInicio;

	@Column(name="bairrofimid")
	private Integer bairroFim;

	private String nome;

	private String complemento;

	private String cep;

	@Column(name="tipologradouro")
	private String tipoLogradouro;

	@Column(name="nomeabreviado")
	private String nomeAbreviado;


	public Integer getId(){
		return id;
	}
	
	public void setId(Integer id){
		this.id = id;
	}
	
	public String getUf(){
		return uf;
	}
	
	public void setUf(String uf){
		this.uf = uf;
	}
	
	public String getNome(){
		return nome;
	}
	
	public void setNome(String nome){
		this.nome = nome;
	}
	
	public String getCep(){
		return cep;
	}
	
	public void setCep(String cep){
		this.cep = cep;
	}
	
	public String getNomeAbreviado(){
		return nomeAbreviado;
	}
	
	public void setNomeAbreviado(String nomeAbreviado){
		this.nomeAbreviado = nomeAbreviado;
	}
	
	public Integer getLocalidadeId(){
		return localidadeId;
	}
	
	public void setLocalidadeId(Integer localidadeId){
		this.localidadeId = localidadeId;
	}
	
	public Integer getBairroInicio(){
		return bairroInicio;
	}
	
	public void setBairroInicio(Integer bairroInicio){
		this.bairroInicio = bairroInicio;
	}
	
	public Integer getBairroFim(){
		return bairroFim;
	}
	
	public void setBairroFim(Integer bairroFim){
		this.bairroFim = bairroFim;
	}
	
	public String getComplemento(){
		return complemento;
	}
	
	public void setComplemento(String complemento){
		this.complemento = complemento;
	}
	
	public String getTipoLogradouro(){
		return tipoLogradouro;
	}
	
	public void setTipoLogradouro(String tipoLogradouro){
		this.tipoLogradouro = tipoLogradouro;
	}
}