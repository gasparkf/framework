package br.com.gaspar.framework.cep.bo;

import java.io.Serializable;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

import br.com.gaspar.framework.cep.entidade.CEPBairro;
import br.com.gaspar.framework.cep.entidade.CEPLocalidade;
import br.com.gaspar.framework.cep.entidade.CEPLogradouro;
import br.com.gaspar.utils.exception.BaseException;

public abstract class BaseCEPBO implements Serializable {

	private static final long serialVersionUID = 1L;

	public abstract EntityManager getEntityManager();

	public CEPLogradouro buscarLogradouro(String cep) throws BaseException {
		CEPLogradouro retorno = null;
       	try{            
       		retorno = (CEPLogradouro)getEntityManager().createQuery("select obj from CEPLogradouro obj where obj.cep = :p0").setParameter("p0", cep).getSingleResult();
        }catch(NoResultException e){
        	retorno = null;
		}
		return retorno;
    }

	@SuppressWarnings("unchecked")
	public List<CEPBairro> buscarBairro(Integer localidade) throws BaseException{
		List<CEPBairro> retorno = null;
		try{
			retorno = getEntityManager().createQuery("select obj from CEPBairro obj where obj.localidadeId = :p0").setParameter("p0", localidade).getResultList();
        }catch(NoResultException e){
        	retorno = null;
        }
		return retorno;
	}

	public CEPBairro buscarBairroPorId(Integer bairro) throws BaseException{
		CEPBairro retorno = null;
		try{
			retorno = (CEPBairro)getEntityManager().createQuery("select obj from CEPBairro obj where obj.id = :p0").setParameter("p0", bairro).getSingleResult();
		}catch(NoResultException e){
			retorno = null;
		}
		return retorno;
	}

	public CEPLocalidade buscarLocalidade(String cep) throws BaseException{
		CEPLocalidade retorno = null;
		try{
			retorno = (CEPLocalidade)getEntityManager().createQuery("select obj from CEPLocalidade obj where obj.cep = :p0").setParameter("p0", cep).getSingleResult();
		}catch(NoResultException e){
			retorno = null;
		}
		return retorno;
	}

	public CEPLocalidade buscarLocalidade(Integer localidade) throws BaseException{
		CEPLocalidade retorno = null;
		try{
			retorno = (CEPLocalidade)getEntityManager().createQuery("select obj from CEPLocalidade obj where obj.id = :p0").setParameter("p0", localidade).getSingleResult();
		}catch(NoResultException e){
			retorno = null;
		}
		return retorno;
	}
}