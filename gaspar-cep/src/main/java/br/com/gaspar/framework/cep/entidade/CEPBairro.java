package br.com.gaspar.framework.cep.entidade;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="cep_bairro")
public class CEPBairro implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="bairroid")
	private Integer id;

	@Column(name="siglauf")
	private String uf;

	@Column(name="localidadeid")
	private Integer localidadeId;

	private String nome;

	@Column(name="nomeabreviado")
	private String nomeAbreviado;


	public Integer getId(){
		return id;
	}
	
	public void setId(Integer id){
		this.id = id;
	}
	
	public String getUf(){
		return uf;
	}
	
	public void setUf(String uf){
		this.uf = uf;
	}
	
	public String getNome(){
		return nome;
	}
	
	public void setNome(String nome){
		this.nome = nome;
	}
	
	public String getNomeAbreviado(){
		return nomeAbreviado;
	}
	
	public void setNomeAbreviado(String nomeAbreviado){
		this.nomeAbreviado = nomeAbreviado;
	}
	
	public Integer getLocalidadeId(){
		return localidadeId;
	}
	
	public void setLocalidadeId(Integer localidadeId){
		this.localidadeId = localidadeId;
	}
}